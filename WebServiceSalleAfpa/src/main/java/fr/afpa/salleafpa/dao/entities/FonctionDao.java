package fr.afpa.salleafpa.dao.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Proxy;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(of= {"id","libelle"})

@Entity
@Table(name = "fonction")
public class FonctionDao {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "fonction_generator")
	@SequenceGenerator(name = "fonction_generator", sequenceName = "seq_fonction",allocationSize = 1)
	@Column (name="id_fonction", updatable = false, nullable = false )
	Integer id;
	
	@Column (name="libelle", updatable = true, nullable = false,length = 50)
	String libelle;
	
	@OneToMany(mappedBy = "fonction",fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	List<PersonneDao> listePersonnes;
	
}
