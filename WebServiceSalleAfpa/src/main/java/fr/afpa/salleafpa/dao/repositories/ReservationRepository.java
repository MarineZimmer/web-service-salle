package fr.afpa.salleafpa.dao.repositories;

import java.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.afpa.salleafpa.dao.entities.ReservationDao;

public interface ReservationRepository extends JpaRepository <ReservationDao, Integer>{

	@Query("SELECT r FROM ReservationDao r WHERE r.dateFin >= ?1")
	Page<ReservationDao> findByDateFin(LocalDate now, Pageable sortedByDateDebut);

	

}
