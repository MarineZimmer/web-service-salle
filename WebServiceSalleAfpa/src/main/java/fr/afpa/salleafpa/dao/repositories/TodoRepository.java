package fr.afpa.salleafpa.dao.repositories;



import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.salleafpa.dao.entities.RoleDao;
import fr.afpa.salleafpa.dao.entities.Todo;

public interface TodoRepository extends JpaRepository <Todo, Integer> {
	
	

}