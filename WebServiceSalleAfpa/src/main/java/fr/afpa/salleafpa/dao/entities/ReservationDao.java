package fr.afpa.salleafpa.dao.entities;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "reservation")
public class ReservationDao {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reservation_generator")
	@SequenceGenerator(name = "reservation_generator", sequenceName = "seq_reservation", allocationSize = 1)
	@Column(name = "id_reservation", updatable = false, nullable = false)
	Integer id;

	@Column(name = "date_debut", updatable = true, nullable = false)
	LocalDate dateDebut;

	@Column(name = "date_fin", updatable = true, nullable = false)
	LocalDate dateFin;

	@Column(name = "nom_reservation", updatable = true, nullable = false, length = 50)
	String nomReservation;

	@ManyToOne(cascade = { CascadeType.PERSIST },fetch = FetchType.EAGER)
	@JoinColumn(name = "id_salle")
	SalleDao salleDao;

	public ReservationDao(int idReservation, LocalDate dateDebut, LocalDate dateFin, String nomReservation) {
		super();
		this.id = idReservation;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.nomReservation = nomReservation;
	}

}
