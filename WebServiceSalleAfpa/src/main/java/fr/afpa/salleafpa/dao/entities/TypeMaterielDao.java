package fr.afpa.salleafpa.dao.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Proxy;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table (name="type_materiel")
public class TypeMaterielDao {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "type_materiel_generator")
	@SequenceGenerator(name = "type_materiel_generator", sequenceName = "seq_type_materiel",allocationSize = 1)
	@Column (name="id_type_materiel", updatable = false, nullable = false )
	Integer id;
	
	@Column (name="libelle", updatable = true, nullable = false,length = 50)
	String libelle;
	
	@OneToMany(mappedBy = "typeMaterielDao",fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	List<MaterielDao> listeMAterielDao;
	

}
