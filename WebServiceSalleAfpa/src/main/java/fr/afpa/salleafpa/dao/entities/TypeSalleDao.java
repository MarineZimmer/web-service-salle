package fr.afpa.salleafpa.dao.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Proxy;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table (name="type_salle")
public class TypeSalleDao {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "type_salle_generator")
	@SequenceGenerator(name = "type_salle_generator", sequenceName = "seq_type_salle",allocationSize = 1)
	@Column (name="id_type", updatable = false, nullable = false )
	Integer id;
	
	@Column (name="libelle", updatable = true, nullable = false,length = 50)
	String libelle;
	
	@OneToMany(mappedBy = "typeSalleDao")
	List<SalleDao> listeSalle;

}
