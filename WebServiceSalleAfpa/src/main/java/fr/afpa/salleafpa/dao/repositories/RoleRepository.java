package fr.afpa.salleafpa.dao.repositories;



import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.salleafpa.dao.entities.RoleDao;

public interface RoleRepository extends JpaRepository <RoleDao, Integer> {
	
	

}