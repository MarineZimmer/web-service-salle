package fr.afpa.salleafpa.dto.services;

import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.afpa.salleafpa.dao.entities.MaterielDao;
import fr.afpa.salleafpa.dao.entities.ReservationDao;
import fr.afpa.salleafpa.dao.entities.SalleDao;
import fr.afpa.salleafpa.dao.repositories.ServiceSalleRepository;
import fr.afpa.salleafpa.dto.iservices.IServiceSalleDto;
import fr.afpa.salleafpa.metier.entities.Materiel;
import fr.afpa.salleafpa.metier.entities.Reservation;
import fr.afpa.salleafpa.metier.entities.Salle;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class ServiceSalleDto implements IServiceSalleDto {

	@Autowired
	private ServiceSalleRepository servSalleRep;

	/**
	 * Methode pour transformer une salle persistante et la transformer en salle
	 * metier
	 * 
	 * @param salleDao : salle DAO recuperée dans la base de données
	 * @return : la salle metier
	 */
	public static Salle salleDaoToSalle(SalleDao salleDao) {
		Salle salle = new Salle(salleDao.getIdSalle(), salleDao.getNumero(), salleDao.getNom(), salleDao.getSurface(),
				salleDao.getCapacite(), salleDao.getEtage(), salleDao.isActif());
		
		salle.setTypeSalle(ServiceTypeSalleDto.typeSalleDaoToTypeSalleMetier(salleDao.getTypeSalleDao()));

		List<Materiel> listeMateriel = salleDao.getListeMaterielDao().stream()
				.map(ServiceMaterielDto::materielDaoToMateriel).collect(Collectors.toList());
		List<Reservation> listeReservation = salleDao.getListeReservationDao().stream()
				.map(ServiceReservationDto::reservationDaoToReservation)
				.sorted((r1, r2) -> r1.getDateDebut().compareTo(r2.getDateDebut())).collect(Collectors.toList());
		salle.setBatiment(ServiceBatimentDto.batimentDaoToBatimentMetier(salleDao.getBatimentDao()));
		salle.setListeMateriel(listeMateriel);
		salle.setListeReservation(listeReservation);
		return salle;
	}

	/**
	 * Methode pour transformer une salle metier et la transformer en salle Dao
	 * 
	 * @param salleDao : salle DAO recuperée dans la base de données
	 * @return : la salle metier
	 */
	public static SalleDao salleToSalleDao(Salle salle) {
		
		SalleDao salleDao = new SalleDao(salle.getIdSalle(), salle.getNumero(), salle.getNom(), salle.getSurface(),
				salle.getCapacite(), salle.getEtage(), salle.isActif());

		salleDao.setTypeSalleDao(ServiceTypeSalleDto.typeSalleMetierToTypeSalleDao(salle.getTypeSalle()));

		
		salleDao.setBatimentDao(ServiceBatimentDto.batimentMetierToBatimentDao(salle.getBatiment()));

		List<MaterielDao> listeMaterielDao = salle.getListeMateriel().stream()
				.map(ServiceMaterielDto::materielToMaterielDao).collect(Collectors.toList());
		
		List<ReservationDao> listeReservationDao = salle.getListeReservation().stream()
				.map(ServiceReservationDto::reservationToReservationDao).collect(Collectors.toList());

		salleDao.setListeMaterielDao(listeMaterielDao);
		salleDao.setListeReservationDao(listeReservationDao);

		return salleDao;
	}

	@Override
	@Transactional
	public List<Salle> getAllSalles() {

		List<SalleDao> listeSalleDao = servSalleRep.findAll();
		List<Salle> listeSalle = listeSalleDao.stream().map(ServiceSalleDto::salleDaoToSalle)
				.collect(Collectors.toList());
		return listeSalle;

	}

	/**
	 * Methode pour créer une nouvelle salle dans la base de données
	 * @param salle: la salle qui sera transfomré et enregistré dans la base de données
	 */
	@Override
	@Transactional
	public Salle createSalle(Salle salle) {
		SalleDao salleDao = salleToSalleDao(salle);
		SalleDao salleDaoCreer=null;
		salleDao.getListeMaterielDao().forEach(e -> e.setSalleDao(salleDao));
		int id = 0;
		try {
			salleDaoCreer= servSalleRep.save(salleDao);
		}catch (Exception e) {
			Logger.getLogger(ServiceSalleDto.class.getName()).log(Level.SEVERE, null, e);
		}
		

		return salleDaoToSalle(salleDaoCreer);

	}

	/**
	 * Methode pour recuperer la salleDao via son id
	 */
	@Override
	@Transactional
	public Salle getSalle(int id) {
		Optional<SalleDao> salleDao = servSalleRep.findById(id);

		return salleDaoToSalle(salleDao.get());
	}

	/**
	 * Methode pour modifier la salle 
	 * @param salle: la salle modifié 
	 * @return boolean: vrai si la salle à bien été enregistré, faux dans le cas contriaire
	 */
	@Override
	public boolean updateSalle(Salle salle) {
		SalleDao salleDao =salleToSalleDao(salle);
		salleDao.getListeMaterielDao().forEach(e -> e.setSalleDao(salleDao));
		boolean flag = false;
		try {
			servSalleRep.save(salleDao);
			flag=true;
		}catch (Exception e) {
			Logger.getLogger(ServiceSalleDto.class.getName()).log(Level.SEVERE, null, e);
		}
		
		return flag;
	}
}
