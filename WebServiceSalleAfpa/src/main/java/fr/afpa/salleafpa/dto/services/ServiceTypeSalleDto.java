package fr.afpa.salleafpa.dto.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dao.entities.BatimentDao;
import fr.afpa.salleafpa.dao.entities.SalleDao;
import fr.afpa.salleafpa.dao.entities.TypeSalleDao;
import fr.afpa.salleafpa.dao.repositories.ServiceTypeSalleRepository;
import fr.afpa.salleafpa.dto.iservices.IServiceTypeSalleDto;
import fr.afpa.salleafpa.metier.entities.Batiment;
import fr.afpa.salleafpa.metier.entities.TypeSalle;

@Service
public class ServiceTypeSalleDto implements IServiceTypeSalleDto {

	@Autowired
	private ServiceTypeSalleRepository typeSalleRepository;

	@Override
	public TypeSalle creation(TypeSalle typeSalle) {
		TypeSalleDao typeSalleDao = typeSalleMetierToTypeSalleDao(typeSalle);
		typeSalleDao = typeSalleRepository.save(typeSalleDao);
		return typeSalleDaoToTypeSalleMetier(typeSalleDao);
	}

	@Override
	public TypeSalle update(TypeSalle typeSalle) {
		TypeSalleDao typeSalleDao = typeSalleMetierToTypeSalleDao(typeSalle);
		typeSalleDao = typeSalleRepository.saveAndFlush(typeSalleDao);
		return typeSalleDaoToTypeSalleMetier(typeSalleDao);
	}

	@Override
	public TypeSalle get(Integer id) {
		Optional<TypeSalleDao> typeSalleDao = typeSalleRepository.findById(id);
		if (typeSalleDao.isPresent()) {
			return typeSalleDaoToTypeSalleMetier(typeSalleDao.get());
		}
		return null;
	}

	@Override
	public List<TypeSalle> getAll() {
		List<TypeSalleDao> listeTypeSalleDao = typeSalleRepository.findAll();
		return listeTypeSalleDao.stream().map(ServiceTypeSalleDto::typeSalleDaoToTypeSalleMetier).collect(Collectors.toList());
	}

	@Override
	public TypeSalle delete(Integer id) {
		TypeSalle typeSalle = null;
		Optional<TypeSalleDao> typeSalleDao = typeSalleRepository.findById(id);
		if (typeSalleDao.isPresent()) {
			typeSalle = typeSalleDaoToTypeSalleMetier(typeSalleDao.get());
			typeSalleRepository.delete(typeSalleDao.get());
		}
		return typeSalle;
	}

	/**
	 * Methode pour transformer l'entite TypeSalle DAO vers l'entite Type
	 * @param typeSalleDao
	 * @return
	 */
	public static TypeSalle typeSalleDaoToTypeSalleMetier(TypeSalleDao typeSalleDao) {
		return new TypeSalle(typeSalleDao.getId(), typeSalleDao.getLibelle());

	}

	/**
	 * Methode pour transformer l'entite TypeSalle metier vers TypeSalle DAO
	 * @param typeSalle
	 * @return
	 */
	public static TypeSalleDao typeSalleMetierToTypeSalleDao(TypeSalle typeSalle) {
		return new TypeSalleDao(typeSalle.getId(), typeSalle.getLibelle(), new ArrayList<SalleDao>());

	}

	
	 


}
