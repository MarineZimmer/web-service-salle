package fr.afpa.salleafpa.dto.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.TypeMateriel;

public interface IServiceTypeMaterielDto {
	
	TypeMateriel creationTypeMateriel(TypeMateriel typeMateriel);

	TypeMateriel updateTypeMateriel(TypeMateriel typeMateriel);

	TypeMateriel getTypeMateriel(Integer id);

	List<TypeMateriel> getAllTypeMateriel();

	TypeMateriel deleteTypeMateriel(Integer id);
}
