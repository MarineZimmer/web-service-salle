package fr.afpa.salleafpa.dto.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.Personne;

public interface IServicePersonnenDto {
	
	
	public Personne savePersonneWS(Personne personne);
	
	public Personne updatePersonneWS(Personne personne);
	
	public Personne deletePersonneWS(String login);
	
	/**
	 * lien entre le service de suppression d'une personne et le service Dao qui
	 * supprime la personne
	 * 
	 * @param personne : une entite metier personne
	 * @return : true si la suppresion à été effectué, false sinon
	 */
	public boolean deletePersonne(Personne personne);
	
	/**
	 * Lien entre le service de modification d'une personne et le service dao qui va
	 * mettre à jour la personne Dao dans la base de données
	 * 
	 * @param personne : l'entite metier personne initialisé auparavant
	 * @return : true si l'update s'est à été effectué, false sinon
	 */
	public boolean updatePersonne(Personne personne);
	
	/**
	 * Lien entre le service de création d'une personne et le service dao qui va
	 * créé la personne Dao dans la base de données
	 * 
	 * @param personne
	 * @return true si la personne a été sauvegardée
	 */
	public boolean savePersonne(Personne personne);
	
	/**
	 * service Dto pour récupérer une personne via son login
	 * @param login : le login recherché
	 * @return : la personne correspondant au login ou null si elle n'existe pas
	 */
	public Personne getPersonneByLogin(String login);
	
	
	/**
	 * service DTO qui sert de lien entre le service metier de recuperation de toutes les  personnes
	 et le service  dao, transforme la liste d'entité PersonneDao renvoyé
	 * par le DAO en liste d'entité personne metier avant de la renvoyer au service metier

	 * @return une liste d'entités personnes metiers
	 */
	public List<Personne> getAllPersonnes();

	

}
