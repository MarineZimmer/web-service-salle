package fr.afpa.salleafpa.dto.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dao.entities.FonctionDao;
import fr.afpa.salleafpa.dao.entities.RoleDao;
import fr.afpa.salleafpa.dao.repositories.FonctionRepository;
import fr.afpa.salleafpa.dto.iservices.IServiceFonctionDto;
import fr.afpa.salleafpa.metier.entities.Fonction;
import fr.afpa.salleafpa.metier.entities.Role;

@Service
public class ServiceFonctionDto implements IServiceFonctionDto{

	
	@Autowired
	private FonctionRepository fonctionRepository;

	@Override
	public Fonction create(Fonction fonction) {
		FonctionDao fonctionDao = fonctionMetierToFonctionDao(fonction);
		fonctionDao = fonctionRepository.save(fonctionDao);
		return fonctionDaoToFonctionMetier(fonctionDao);
	}

	@Override
	public Fonction update(Fonction fonction) {
		FonctionDao fonctionDao = fonctionMetierToFonctionDao(fonction);
		fonctionDao = fonctionRepository.saveAndFlush(fonctionDao);
		return fonctionDaoToFonctionMetier(fonctionDao);
	}

	@Override
	public Fonction getFonction(Integer id) {
		Optional<FonctionDao> fonctionDao = fonctionRepository.findById(id);
		if (fonctionDao.isPresent()) {
			return fonctionDaoToFonctionMetier(fonctionDao.get());
		}
		return null;
	}

	@Override
	public List<Fonction> getAllFonction() {
		List<FonctionDao> listeFonctionDao = fonctionRepository.findAll();
		return listeFonctionDao.stream().map(ServiceFonctionDto::fonctionDaoToFonctionMetier).collect(Collectors.toList());
	}

	@Override
	public Fonction delete(Integer id) {
		Fonction fonction = null;
		Optional<FonctionDao> fonctionDao = fonctionRepository.findById(id);
		if (fonctionDao.isPresent()) {
			fonction = fonctionDaoToFonctionMetier(fonctionDao.get());
			fonctionRepository.delete(fonctionDao.get());
		}
		return fonction;
	}
	
	
	
	
	
	
	/**
	 * méthode qui permet de transformer une entité fonction metier en entité
	 * fonctionDao
	 * 
	 * @param fonction : entité fonction metier à transformer
	 * @return : une entité fonctionDao correspondante à l'entite fonction metier
	 */
	public static FonctionDao fonctionMetierToFonctionDao(Fonction fonction) {
		FonctionDao fonctionDao = new FonctionDao();
		fonctionDao.setId(fonction.getId());
		fonctionDao.setLibelle(fonction.getLibelle());
	
		return fonctionDao;
	}

	/**
	 * methode qui permet de transformer une entité fonctionDao en entité fonction
	 * metier
	 * 
	 * @param fonctionDao : entité fonctionDao à transformer
	 * @return : une entité fonction metier correspondante à l'entité fonctionDao
	 */
	public static Fonction fonctionDaoToFonctionMetier(FonctionDao fonctionDao) {
		Fonction fonction = new Fonction(fonctionDao.getId(), fonctionDao.getLibelle());
		return fonction;
	}

}
