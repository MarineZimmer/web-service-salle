package fr.afpa.salleafpa.dto.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dao.entities.BatimentDao;
import fr.afpa.salleafpa.dao.entities.RoleDao;
import fr.afpa.salleafpa.dao.entities.SalleDao;
import fr.afpa.salleafpa.dao.repositories.BatimentRepository;
import fr.afpa.salleafpa.dto.iservices.IServiceBatimentDto;
import fr.afpa.salleafpa.metier.entities.Batiment;
import fr.afpa.salleafpa.metier.entities.Role;

@Service
public class ServiceBatimentDto implements IServiceBatimentDto {

	@Autowired
	private BatimentRepository batimentRepository;

	@Override
	public Batiment creation(Batiment batiment) {
		BatimentDao batimentDao = batimentMetierToBatimentDao(batiment);
		batimentDao = batimentRepository.save(batimentDao);
		return batimentDaoToBatimentMetier(batimentDao);
	}

	@Override
	public Batiment update(Batiment batiment) {
		BatimentDao batimentDao = batimentMetierToBatimentDao(batiment);
		batimentDao = batimentRepository.saveAndFlush(batimentDao);
		return batimentDaoToBatimentMetier(batimentDao);
	}

	@Override
	public Batiment get(Integer id) {
		Optional<BatimentDao> batimentDao = batimentRepository.findById(id);
		if (batimentDao.isPresent()) {
			return batimentDaoToBatimentMetier(batimentDao.get());
		}
		return null;
	}

	@Override
	public List<Batiment> getAll() {
		List<BatimentDao> listeBatimentDao = batimentRepository.findAll();
		return listeBatimentDao.stream().map(ServiceBatimentDto::batimentDaoToBatimentMetier).collect(Collectors.toList());
	}

	@Override
	public Batiment delete(Integer id) {
		Batiment batiment = null;
		Optional<BatimentDao> batimentDao = batimentRepository.findById(id);
		if (batimentDao.isPresent()) {
			batiment = batimentDaoToBatimentMetier(batimentDao.get());
			batimentRepository.delete(batimentDao.get());
		}
		return batiment;
	}
	public static Batiment batimentDaoToBatimentMetier(BatimentDao batimentDao) {
		Batiment batiment = new Batiment();
		batiment.setId(batimentDao.getId());
		batiment.setLibelle(batimentDao.getLibelle());
		return batiment;

	}
	public static BatimentDao batimentMetierToBatimentDao(Batiment batiment) {
		BatimentDao batimentDao = new BatimentDao();
		batimentDao.setId(batiment.getId());
		batimentDao.setLibelle(batiment.getLibelle());
		List<SalleDao> listeSalleDao = new ArrayList<SalleDao>();
		batimentDao.setListeSalle(listeSalleDao);

		return batimentDao;

	}

	
}