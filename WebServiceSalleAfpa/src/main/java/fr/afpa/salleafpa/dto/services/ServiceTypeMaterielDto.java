package fr.afpa.salleafpa.dto.services;

import java.util.ArrayList;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dao.entities.MaterielDao;
import fr.afpa.salleafpa.dao.entities.RoleDao;
import fr.afpa.salleafpa.dao.entities.TypeMaterielDao;
import fr.afpa.salleafpa.dao.repositories.TypeMaterielRepository;
import fr.afpa.salleafpa.dto.iservices.IServiceTypeMaterielDto;
import fr.afpa.salleafpa.metier.entities.Role;
import fr.afpa.salleafpa.metier.entities.TypeMateriel;
@Service
public class ServiceTypeMaterielDto implements IServiceTypeMaterielDto{
	@Autowired
	private TypeMaterielRepository typeMaterielRepository;
	
	@Override
	public TypeMateriel creationTypeMateriel(TypeMateriel typeMateriel) {
		TypeMaterielDao typeMaterielDao = typeMaterielMetierToTypeMaterielDao(typeMateriel);
		typeMaterielDao = typeMaterielRepository.save(typeMaterielDao);
		return typeMaterielDaoToTypeMaterielMetier(typeMaterielDao);
	}

	@Override
	public TypeMateriel updateTypeMateriel(TypeMateriel typeMateriel) {
		TypeMaterielDao typeMaterielDao = typeMaterielMetierToTypeMaterielDao(typeMateriel);
		typeMaterielDao =typeMaterielRepository.saveAndFlush(typeMaterielDao);
		return typeMaterielDaoToTypeMaterielMetier(typeMaterielDao);
	}

	@Override
	public TypeMateriel getTypeMateriel(Integer id) {
		Optional<TypeMaterielDao> typeMaterielDao = typeMaterielRepository.findById(id);
		if (typeMaterielDao.isPresent()) {
			return typeMaterielDaoToTypeMaterielMetier(typeMaterielDao.get());
		}
		return null;
	}

	@Override
	public List<TypeMateriel> getAllTypeMateriel() {
		List<TypeMaterielDao> listeTypeMaterielDao = typeMaterielRepository.findAll();
		return listeTypeMaterielDao.stream().map(ServiceTypeMaterielDto::typeMaterielDaoToTypeMaterielMetier).collect(Collectors.toList());
	}

	@Override
	public TypeMateriel deleteTypeMateriel(Integer id) {
		TypeMateriel typeMateriel = null;
		Optional<TypeMaterielDao> typeMaterielDao = typeMaterielRepository.findById(id);
		if (typeMaterielDao.isPresent()) {
			typeMateriel = typeMaterielDaoToTypeMaterielMetier(typeMaterielDao.get());
			typeMaterielRepository.delete(typeMaterielDao.get());
		}
		return typeMateriel;
	}
	/**
	 * Methode pour transformer l'entite TypeMateriel DAO vers l'entite Type
	 * @param typeMaterielDao
	 * @return
	 */
	public static TypeMateriel typeMaterielDaoToTypeMaterielMetier(TypeMaterielDao typeMaterielDao) {
		return new TypeMateriel(typeMaterielDao.getId(), typeMaterielDao.getLibelle());
	}
	/**
	 * Methode pour transformer l'entite TypeMateriel metier vers typeMateriel DAO
	 * @param typeMateriel
	 * @return
	 */
	
	public static TypeMaterielDao typeMaterielMetierToTypeMaterielDao(TypeMateriel typeMateriel) {
		TypeMaterielDao typeMaterielDao =  new TypeMaterielDao();
		typeMaterielDao.setId(typeMateriel.getId());
		typeMaterielDao.setLibelle(typeMateriel.getLibelle());
		typeMaterielDao.setListeMAterielDao(new ArrayList<MaterielDao>());	
		return typeMaterielDao;
	}

	}

