package fr.afpa.salleafpa.dto.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.Role;

public interface IServiceRoleDto {
	Role creationRole(Role role);

	Role updateRole(Role role);

	Role getRole(Integer id);

	List<Role> getAllRole();

	Role deleteRole(Integer id);
}
