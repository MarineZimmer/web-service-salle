package fr.afpa.salleafpa.dto.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.apache.catalina.ha.backend.CollectedInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dao.entities.RoleDao;
import fr.afpa.salleafpa.dao.repositories.RoleRepository;
import fr.afpa.salleafpa.dto.iservices.IServiceRoleDto;
import fr.afpa.salleafpa.metier.entities.Role;

@Service
public class ServiceRoleDto implements IServiceRoleDto {

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public Role creationRole(Role role) {
		RoleDao roleDao = roleMetierToRoleDao(role);
		roleDao = roleRepository.save(roleDao);
		return roleDaoToRoleMetier(roleDao);
	}

	@Override
	public Role updateRole(Role role) {
		RoleDao roleDao = roleMetierToRoleDao(role);
		roleDao = roleRepository.saveAndFlush(roleDao);
		return roleDaoToRoleMetier(roleDao);
	}

	@Override
	public Role getRole(Integer id) {
		Optional<RoleDao> roleDao = roleRepository.findById(id);
		if (roleDao.isPresent()) {
			return roleDaoToRoleMetier(roleDao.get());
		}
		return null;
	}

	@Override
	public List<Role> getAllRole() {
		List<RoleDao> listeRoleDao = roleRepository.findAll();
		return listeRoleDao.stream().map(ServiceRoleDto::roleDaoToRoleMetier).collect(Collectors.toList());
	}

	@Override
	public Role deleteRole(Integer id) {
		Role role = null;
		Optional<RoleDao> roleDao = roleRepository.findById(id);
		if (roleDao.isPresent()) {
			role = roleDaoToRoleMetier(roleDao.get());
			roleRepository.delete(roleDao.get());
		}
		return role;
	}

	/**
	 * méthode qui permet de transformer une entité role metier en entité roleDao
	 * 
	 * @param role : entité role metier à transformer
	 * @return : une entité roleDao correspondante à l'entite role metier
	 */
	public static RoleDao roleMetierToRoleDao(Role role) {
		RoleDao roleDao = new RoleDao();
		roleDao.setId(role.getId());
		roleDao.setLibelle(role.getLibelle());

		return roleDao;
	}

	/**
	 * méthode qui permet de transformer une entité roleDao en entité role metier
	 * 
	 * @param auth : entité roleDao à transformer
	 * @return : une entité role metier correspondante à l'entité roleDao
	 */
	public static Role roleDaoToRoleMetier(RoleDao roleDao) {
		Role role = new Role(roleDao.getId(), roleDao.getLibelle());
		return role;
	}

}
