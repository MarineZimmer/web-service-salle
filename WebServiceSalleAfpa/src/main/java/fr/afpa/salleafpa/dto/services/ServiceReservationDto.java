package fr.afpa.salleafpa.dto.services;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dao.entities.ReservationDao;
import fr.afpa.salleafpa.dao.entities.SalleDao;
import fr.afpa.salleafpa.dao.repositories.ReservationRepository;
import fr.afpa.salleafpa.dto.iservices.IServiceReservationDto;
import fr.afpa.salleafpa.dto.iservices.IServiceSalleDto;
import fr.afpa.salleafpa.metier.entities.Reservation;
import fr.afpa.salleafpa.metier.entities.Salle;
import fr.afpa.salleafpa.outils.ControleSaisie;

@Service
public class ServiceReservationDto implements IServiceReservationDto {

	@Autowired
	private ReservationRepository reservationRepository;

	@Autowired
	private IServiceSalleDto salleDto;

	@Override
	public Reservation createReservation(Reservation reservation) {
		ReservationDao reservationDao = reservationToReservationDao(reservation);
		try {
			reservationDao = reservationRepository.save(reservationDao);
			return reservationDaoToReservation(reservationDao);

		} catch (Exception e) {
			Logger.getLogger(ServiceReservationDto.class.getName()).log(Level.SEVERE, null, e);
			return null;
		}
	}

	@Override
	public Reservation update(Reservation reservation) {
		ReservationDao reservationDao = reservationToReservationDao(reservation);
		try {
			reservationRepository.save(reservationDao);
			return reservationDaoToReservation(reservationDao);

		} catch (Exception e) {
			Logger.getLogger(ServiceReservationDto.class.getName()).log(Level.SEVERE, null, e);
			return null;
		}
	}

	@Override
	public Reservation getById(int id) {
		Optional<ReservationDao> reservationDao = reservationRepository.findById(id);
		if (reservationDao.isPresent()) {
			Reservation reservation = reservationDaoToReservation(reservationDao.get());
			Salle salle = salleDto.getSalle(reservationDao.get().getSalleDao().getIdSalle());
			reservation.setSalle(salle);
			return reservation;
		}
		return null;
	}

	@Override
	public List<Reservation> getAll(int page, int nbReservPage) {

		Pageable sortedByDateDebut = PageRequest.of(page, nbReservPage, Sort.by("dateDebut").and(Sort.by("dateFin")));
		Page<ReservationDao> listeReservationDao = reservationRepository.findByDateFin(LocalDate.now(),
				sortedByDateDebut);
		List<Reservation> listeReservation = new ArrayList<Reservation>();

		listeReservationDao.forEach(r -> {
			Reservation reservation = reservationDaoToReservation(r);
			Salle salle = salleDto.getSalle(r.getSalleDao().getIdSalle());
			reservation.setSalle(salle);
			listeReservation.add(reservation);
		});
		return listeReservation;
	}

	@Override
	public int nbPageListeReservation(int nbReservPage) {
		Pageable sortedByDateDebut = PageRequest.of(0, nbReservPage, Sort.by("dateDebut").and(Sort.by("dateFin")));
		return reservationRepository.findByDateFin(LocalDate.now(), sortedByDateDebut).getTotalPages();
	}

	@Override
	public void deleteById(int id) {
		reservationRepository.deleteById(id);

	}

	/**
	 * service de mapping entité ReservationDAO/entité métier Reservation
	 * 
	 * @param reservationDao entité ReservationDAO à transformé
	 * @return l'entité Reservation métier
	 */
	public static Reservation reservationDaoToReservation(ReservationDao reservationDao) {
		Reservation reservation = new Reservation(reservationDao.getId(), reservationDao.getDateDebut(),
				reservationDao.getDateFin(), reservationDao.getNomReservation());
		return reservation;
	}

	/**
	 * service de mapping entité métier Reservation/ReservationDAO
	 * 
	 * @param reservationDao entité métier Reservation à transformé
	 * @return l'entité ReservationDao
	 */
	public static ReservationDao reservationToReservationDao(Reservation reservation) {
		ReservationDao reservationDao = new ReservationDao(reservation.getId(), reservation.getDateDebut(),
				reservation.getDateFin(), reservation.getNomReservation());
		//reservationDao.setSalleDao(new SalleDao(reservation.getSalle().getIdSalle()));
		return reservationDao;
	}

}
