package fr.afpa.salleafpa.dto.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.Batiment;
import fr.afpa.salleafpa.metier.entities.Role;

public interface IServiceBatimentDto {
	
	Batiment creation(Batiment batiment);

	Batiment update(Batiment batiment);

	Batiment get(Integer id);

	List<Batiment> getAll();

	Batiment delete(Integer id);
}