package fr.afpa.salleafpa.dto.iservices;

import java.util.List;
import fr.afpa.salleafpa.metier.entities.Salle;

public interface IServiceSalleDto {
	
	/**
	 * Methode pour retourner une liste de Salles
	 * @return retourne une liste de toutes les salles dans la base de données
	 */
	public List<Salle> getAllSalles();
	
	/**
	 * Methode qui va créer une salle
	 * @param salle
	 * @return
	 */
	public Salle createSalle(Salle salle);
	
	
	/**
	 * Service qui recupere la salle via son id;
	 * @param id : identifiant de la salle;
	 * @return la salle trouvé
	 */
	public Salle getSalle(int id);
	
	/**
	 * Service pour mettre à jour la salle
	 * @param salle: salle à modifier
	 * @return 
	 */
	public boolean updateSalle(Salle salle);
}
