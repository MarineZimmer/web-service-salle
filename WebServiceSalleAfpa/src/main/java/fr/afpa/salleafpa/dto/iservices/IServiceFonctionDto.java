package fr.afpa.salleafpa.dto.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.Fonction;

public interface IServiceFonctionDto {
	Fonction create(Fonction fonction);

	Fonction update(Fonction fonction);

	Fonction getFonction(Integer id);

	List<Fonction> getAllFonction();

	Fonction delete(Integer id);
}
