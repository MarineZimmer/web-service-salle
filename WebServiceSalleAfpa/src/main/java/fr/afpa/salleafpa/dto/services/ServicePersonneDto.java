package fr.afpa.salleafpa.dto.services;

import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.afpa.salleafpa.dao.entities.FonctionDao;
import fr.afpa.salleafpa.dao.entities.PersonneDao;
import fr.afpa.salleafpa.dao.entities.RoleDao;
import fr.afpa.salleafpa.dao.repositories.FonctionRepository;
import fr.afpa.salleafpa.dao.repositories.PersonneRepository;
import fr.afpa.salleafpa.dao.repositories.RoleRepository;
import fr.afpa.salleafpa.dto.iservices.IServicePersonnenDto;
import fr.afpa.salleafpa.metier.entities.Personne;

@Service
public class ServicePersonneDto implements IServicePersonnenDto {

	@Autowired
	private PersonneRepository persRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private FonctionRepository fonctionRepository;

	@Override
	public Personne savePersonneWS(Personne personne) {
		PersonneDao personneDao = personneMetierToPersonneDao(personne);
		try {
			RoleDao role = roleRepository.findById(personneDao.getRole().getId()).orElse(null);
			FonctionDao fonction = fonctionRepository.findById(personneDao.getRole().getId()).orElse(null);
			personneDao.setRole(role);
			personneDao.setFonction(fonction);
			personneDao = persRepository.save(personneDao);
			return personneDaoToPersonneMetier(personneDao);

		} catch (Exception e) {
			Logger.getLogger(ServicePersonneDto.class.getName()).log(Level.SEVERE, null, e);
			return null;
		}

	}

	@Override
	public Personne updatePersonneWS(Personne personne) {
		PersonneDao personneDao = personneMetierToPersonneDao(personne);
		try {
			RoleDao role = roleRepository.findById(personneDao.getRole().getId()).orElse(null);
			FonctionDao fonction = fonctionRepository.findById(personneDao.getRole().getId()).orElse(null);
			personneDao.setRole(role);
			personneDao.setFonction(fonction);
			personneDao = persRepository.saveAndFlush(personneDao);
			return personneDaoToPersonneMetier(personneDao);

		} catch (Exception e) {
			Logger.getLogger(ServicePersonneDto.class.getName()).log(Level.SEVERE, null, e);
			return null;
		}

	}

	@Override
	@Transactional
	public Personne deletePersonneWS(String login) {
		try {
			Optional<PersonneDao> personneDao = persRepository.findByLogin(login);
			if (personneDao.isPresent()) {
				Personne personne = personneDaoToPersonneMetier(personneDao.get());
				persRepository.delete(personneDao.get());
				return personne;
			}

		} catch (Exception e) {
			Logger.getLogger(ServicePersonneDto.class.getName()).log(Level.SEVERE, null, e);

		}
		return null;
	}

	@Override
	public boolean savePersonne(Personne personne) {
		PersonneDao personneDao = personneMetierToPersonneDao(personne);
		try {
			RoleDao role = roleRepository.findById(personneDao.getRole().getId()).get();
			FonctionDao fonction = fonctionRepository.findById(personneDao.getRole().getId()).get();
			personneDao.setRole(role);
			personneDao.setFonction(fonction);
			personneDao = persRepository.save(personneDao);
			return true;
		} catch (Exception e) {
			Logger.getLogger(ServicePersonneDto.class.getName()).log(Level.SEVERE, null, e);
			return false;
		}

	}

	@Override
	public boolean deletePersonne(Personne personne) {
		try {
			PersonneDao personneDao = personneMetierToPersonneDao(personne);
			persRepository.delete(personneDao);
			return true;
		} catch (Exception e) {
			Logger.getLogger(ServicePersonneDto.class.getName()).log(Level.SEVERE, null, e);
			return false;
		}
	}

	@Override
	public boolean updatePersonne(Personne personne) {
		try {
			PersonneDao personneDao = personneMetierToPersonneDao(personne);
			RoleDao role = roleRepository.findById(personneDao.getRole().getId()).get();
			FonctionDao fonction = fonctionRepository.findById(personneDao.getRole().getId()).get();
			personneDao.setRole(role);
			personneDao.setFonction(fonction);
			persRepository.save(personneDao);
			return true;
		} catch (Exception e) {
			Logger.getLogger(ServicePersonneDto.class.getName()).log(Level.SEVERE, null, e);
			return false;
		}
	}

	@Override
	public Personne getPersonneByLogin(String login) {
		System.out.println(login);
		Optional<PersonneDao> personneDao = persRepository.findByLogin(login);
		System.out.println(personneDao);
		if (personneDao.isPresent()) {
			return ServicePersonneDto.personneDaoToPersonneMetier(personneDao.get());
		}
		return null;

	}

	@Override
	public List<Personne> getAllPersonnes() {
		List<PersonneDao> listePersDao = persRepository.findAll();
		List<Personne> listePers = null;
		if (listePersDao != null) {
			listePers = listePersDao.stream().map(ServicePersonneDto::personneDaoToPersonneMetier)
					.collect(Collectors.toList());
		}
		return listePers;
	}

	/**
	 * méthode qui permet de transformer une entité personne metier en entité
	 * personneDao
	 * 
	 * @param personne : entité personne metier à transformer
	 * @return : une entité personneDao correspondante à l'entite personne metier
	 */
	public static PersonneDao personneMetierToPersonneDao(Personne personne) {
		PersonneDao personneDao = new PersonneDao();
		personneDao.setId(personne.getId());
		personneDao.setNom(personne.getNom());
		personneDao.setActif(personne.isActif());
		personneDao.setAdresse(personne.getAdresse());
		personneDao.setAuthentification(ServiceAuthentificationDto.authMetierToAuthDao(personne.getAuthentification()));
		personneDao.setDateDeNaissance(personne.getDateDeNaissance());
		personneDao.setFonction(ServiceFonctionDto.fonctionMetierToFonctionDao(personne.getFonction()));
		personneDao.setMail(personne.getMail());
		personneDao.setPrenom(personne.getPrenom());
		personneDao.setRole(ServiceRoleDto.roleMetierToRoleDao(personne.getRole()));
		personneDao.setTel(personne.getTel());
		return personneDao;
	}

	/**
	 * méthode qui permet de transformer une entité personneDao en entité personne
	 * metier
	 * 
	 * @param personneDao : entité personneDao à transformer
	 * @return : une entité personne métier correspondante à l'entité personneDao
	 */
	public static Personne personneDaoToPersonneMetier(PersonneDao personneDao) {
		Personne personne = new Personne();
		personne.setId(personneDao.getId());
		personne.setNom(personneDao.getNom());
		personne.setActif(personneDao.isActif());
		personne.setAdresse(personneDao.getAdresse());
		personne.setAuthentification(ServiceAuthentificationDto.authDaoToAuthMetier(personneDao.getAuthentification()));
		personne.setDateDeNaissance(personneDao.getDateDeNaissance());
		personne.setFonction(ServiceFonctionDto.fonctionDaoToFonctionMetier(personneDao.getFonction()));
		personne.setMail(personneDao.getMail());
		personne.setPrenom(personneDao.getPrenom());
		personne.setRole(ServiceRoleDto.roleDaoToRoleMetier(personneDao.getRole()));
		personne.setTel(personneDao.getTel());

		return personne;
	}

}
