package fr.afpa.salleafpa.jwt;

import java.security.Key;
import java.time.Instant;
import java.util.Date;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

public class TestJwt {
	
	// The secret key. This should be in a property file NOT under source
    // control and not hard coded in real life. We're putting it here for
    // simplicity.
    private static SecretKey SECRET_KEY = Keys.secretKeyFor(SignatureAlgorithm.HS256);
	
    public static String createJWT(String id, String issuer, String subject, long ttlMillis) {
    	
    	String jws = Jwts.builder()
    			  .setIssuer("Stormpath")
    			  .setAudience("audience")
    			  .setSubject("msilverman")
    			  .claim("name", "Micah Silverman")
    			  .claim("scope", "admins")
    			  .claim("role","admin")
    			  // Fri Jun 24 2016 15:33:42 GMT-0400 (EDT)
    			  .setIssuedAt(Date.from(Instant.ofEpochSecond(1466796822L)))
    			  // Sat Jun 24 2116 15:33:42 GMT-0400 (EDT)
    			  .setExpiration(Date.from(Instant.ofEpochSecond(4622470422L)))
    			  .signWith(
    			   SECRET_KEY
    			  )
    			  .compact();
    	
		  return jws;
	 /*   //The JWT signature algorithm we will be using to sign the token
	    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

	    long nowMillis = System.currentTimeMillis();
	    Date now = new Date(nowMillis);

	    //We will sign our JWT with our ApiKey secret
	   // byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
	   // Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

	    //Let's set the JWT Claims
	    JwtBuilder builder = Jwts.builder().setId(id)
	            .setIssuedAt(now)
	            .setSubject(subject)
	            .setIssuer(issuer)
	            .signWith(signatureAlgorithm, SECRET_KEY);
	  
	    //if it has been specified, let's add the expiration
	    if (ttlMillis > 0) {
	        long expMillis = nowMillis + ttlMillis;
	        Date exp = new Date(expMillis);
	        builder.setExpiration(exp);
	    }  
	  
	    //Builds the JWT and serializes it to a compact, URL-safe string
	    return builder.compact();*/
	}
    
    public static Jwt decodeJWT(String jwt) {
    	
    	 
        //This line will throw an exception if it is not a signed JWS (as expected)
        Claims claims = Jwts.parser()
                .setSigningKey(SECRET_KEY)
                .parseClaimsJws(jwt).getBody();
        
        Jwt claims2 = Jwts.parserBuilder()
                .requireAudience("audience").require("role", "admin").setSigningKey(SECRET_KEY)
                .build()
                .parse(jwt);
        		return claims2;

    }
}
