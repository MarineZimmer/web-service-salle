package fr.afpa.salleafpa.metier.services;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.metier.entities.Salle;
import fr.afpa.salleafpa.metier.iservices.IServiceFiltreSalle;

@Service
public class ServiceFiltreSalle implements IServiceFiltreSalle {

	@Override
	public List<Salle> filtreDateSalle(List<Salle> listeSalle, LocalDate dateDebut, LocalDate dateFin, boolean libre) {
		return listeSalle.stream()
				.filter(s->etatSalle(s, dateDebut, dateFin, libre))
				.collect(Collectors.toList());

	}

	@Override
	public List<Salle> filtreTypeSalle(List<Salle> listeSalle, List<Integer> listeId) {

		return listeSalle.stream().filter(s -> listeId.contains(s.getTypeSalle().getId())).collect(Collectors.toList());
	}

	@Override
	public List<Salle> filtreMateriel(List<Salle> listeSalle, int id, int quantite) {

		return listeSalle.stream()
				.filter(e -> e.getListeMateriel().stream()
						.anyMatch(m -> m.getTypeMateriel().getId() == id && m.getQuantite() >= quantite))
				.collect(Collectors.toList());
	}

	@Override
	public List<Salle> filtreCapacite(List<Salle> listeSalle, int min) {

		return listeSalle.stream().filter(s -> s.getCapacite() >= min).collect(Collectors.toList());

	}
	
	@Override
	public boolean etatSalle(Salle salle, LocalDate dateDebut, LocalDate dateFin, boolean libre ) {
		return salle.getListeReservation().stream().allMatch(
				r -> dateDebut.isAfter(r.getDateFin()) || dateFin.isBefore(r.getDateDebut())) == libre;
	}
	
	
	@Override
	public List<Salle> filtreActif(List<Salle> listeSalle, boolean actif) {

		return listeSalle.stream().filter(s -> s.isActif()==actif).collect(Collectors.toList());

	}

}
