package fr.afpa.salleafpa.metier.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.Reservation;

public interface IServiceReservation {

	/**
	 * service metier pour la sauvegarde d'une réservation
	 * 
	 * @param reservation : la réservation à sauvegarder
	 * @return la réservation sauvegardée
	 */
	public Reservation createReservation(Reservation reservation);

	/**
	 * service metier pour récupérer une réservation via l'id
	 * 
	 * @param id : l'id de la reservation
	 * @return : la réservation corespondant à l'id
	 */
	public Reservation getById(int id);

	/**
	 * service metier pour l'update d'une réservation
	 * 
	 * @param reservation : la reservation à modifier
	 * @return la reservation modifiée qui a été sauvegardée
	 */
	public Reservation update(Reservation reservation);

	/**
	 * service pour retourner l'ensemble des réservations sous forme de page (le
	 * nombre de réservation par page est configurable dans la classe Parametrage)
	 * 
	 * @param page         le numero de page
	 * @param nbReservPage le nombre de réservation par page
	 * @return une liste correspondant aux résevations à afficher
	 */
	public List<Reservation> getAll(int page, int nbReservPage);

	/**
	 * service qui retourne le nombre de page necessaire pour afficher les
	 * réservations
	 * 
	 * @param nbReservPage : le nombre de réservations par page
	 * @return le nombre de page pour afficher les reservation
	 */
	public int nbPageListeReservation(int nbReservPage);

	/**
	 * Supprime une réservation via son id
	 * 
	 * @param id : l'id de la réservation à supprimer
	 */
	public void deleteById(int id);
}
