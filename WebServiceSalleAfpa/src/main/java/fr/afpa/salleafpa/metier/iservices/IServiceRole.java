package fr.afpa.salleafpa.metier.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.Role;

public interface IServiceRole {

	Role creationRole(Role role);

	Role updateRole(Role role);

	Role getRole(Integer id);

	List<Role> getAllRole();

	Role deleteRole(Integer id);


}
