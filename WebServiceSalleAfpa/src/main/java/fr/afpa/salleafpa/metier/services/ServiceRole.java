package fr.afpa.salleafpa.metier.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dto.iservices.IServiceRoleDto;
import fr.afpa.salleafpa.metier.entities.Role;
import fr.afpa.salleafpa.metier.iservices.IServiceRole;

@Service
public class ServiceRole implements IServiceRole{
	
	@Autowired
	private IServiceRoleDto iServRoleDto;

	@Override
	public Role creationRole(Role role) {
	
		return iServRoleDto.creationRole(role);
	}

	@Override
	public Role updateRole(Role role) {
		return iServRoleDto.updateRole(role);
	}

	@Override
	public Role getRole(Integer id) {
		return iServRoleDto.getRole(id);
	}

	@Override
	public List<Role> getAllRole() {
		
		return iServRoleDto.getAllRole();
	}

	@Override
	public Role deleteRole(Integer id) {
		return iServRoleDto.deleteRole(id);
	}

}
