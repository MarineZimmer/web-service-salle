package fr.afpa.salleafpa.metier.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dto.iservices.IServiceTypeMaterielDto;
import fr.afpa.salleafpa.metier.entities.TypeMateriel;
import fr.afpa.salleafpa.metier.iservices.IServiceTypeMateriel;
@Service
public class ServiceTypeMateriel implements IServiceTypeMateriel{
	@Autowired
	private IServiceTypeMaterielDto iServTypeMaterielDto;
	
		@Override
		public TypeMateriel creationTypeMateriel(TypeMateriel typeMateriel) {
		
			return iServTypeMaterielDto.creationTypeMateriel(typeMateriel);
		}

		@Override
		public TypeMateriel updateTypeMateriel(TypeMateriel typeMateriel) {
			return iServTypeMaterielDto.updateTypeMateriel(typeMateriel);
		}

		@Override
		public TypeMateriel getTypeMateriel(Integer id) {
			return iServTypeMaterielDto.getTypeMateriel(id);
		}

		@Override
		public List<TypeMateriel> getAllTypeMateriel() {
			
			return iServTypeMaterielDto.getAllTypeMateriel();
		}

		@Override
		public TypeMateriel deleteTypeMateriel(Integer id) {
			return iServTypeMaterielDto.deleteTypeMateriel(id);
		}

	}



