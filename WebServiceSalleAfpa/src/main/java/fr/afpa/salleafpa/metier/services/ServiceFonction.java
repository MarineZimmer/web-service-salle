package fr.afpa.salleafpa.metier.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dto.iservices.IServiceFonctionDto;
import fr.afpa.salleafpa.metier.entities.Fonction;
import fr.afpa.salleafpa.metier.entities.Role;
import fr.afpa.salleafpa.metier.iservices.IServiceFonction;

@Service
public class ServiceFonction implements IServiceFonction{
	
	@Autowired
	private IServiceFonctionDto iServFonctionDto;

	@Override
	public Fonction create(Fonction fonction) {
	
		return iServFonctionDto.create(fonction);
	}

	@Override
	public Fonction update(Fonction fonction) {
		return iServFonctionDto.update(fonction);
	}

	@Override
	public Fonction getFonction(Integer id) {
		return iServFonctionDto.getFonction(id);
	}

	@Override
	public List<Fonction> getAllFonction() {
		
		return iServFonctionDto.getAllFonction();
	}

	@Override
	public Fonction delete(Integer id) {
		return iServFonctionDto.delete(id);
	}

}
