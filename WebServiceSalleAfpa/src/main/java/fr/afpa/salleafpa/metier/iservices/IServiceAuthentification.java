package fr.afpa.salleafpa.metier.iservices;

import fr.afpa.salleafpa.metier.entities.Personne;

public interface IServiceAuthentification {
	
	/**
	 * Service de controle de l'authentification, envoie au dto une entite
	 * authentification metier, si l'authentification est correcte, le dto renvoie
	 * la personne authentifiée
	 * 
	 * @param login : le login à tester
	 * @param mdp : le mot de passe à tester
	 * @return une entite metier personne si l'authentification est correcte, null sinon
	 */
	public Personne authentification(String login, String mdp);
}
