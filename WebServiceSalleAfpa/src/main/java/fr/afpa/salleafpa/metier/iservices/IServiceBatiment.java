package fr.afpa.salleafpa.metier.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.Batiment;
import fr.afpa.salleafpa.metier.entities.Role;

public interface IServiceBatiment {
	
	Batiment creation(Batiment batiment);

	Batiment update(Batiment batiment);

	Batiment get(Integer id);

	List<Batiment> getAll();

	Batiment delete(Integer id);
}
