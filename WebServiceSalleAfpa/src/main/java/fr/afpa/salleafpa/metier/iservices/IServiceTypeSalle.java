package fr.afpa.salleafpa.metier.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.TypeSalle;


public interface IServiceTypeSalle {
	
	TypeSalle creation(TypeSalle typeSalle);

	TypeSalle update(TypeSalle typeSalle);

	TypeSalle get(Integer id);

	List<TypeSalle> getAll();

	TypeSalle delete(Integer id);
}
