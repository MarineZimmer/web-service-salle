package fr.afpa.salleafpa.metier.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dto.iservices.IServiceBatimentDto;
import fr.afpa.salleafpa.metier.entities.Batiment;
import fr.afpa.salleafpa.metier.entities.Role;
import fr.afpa.salleafpa.metier.iservices.IServiceBatiment;

@Service
public class ServiceBatiment implements IServiceBatiment {

	@Autowired
	private IServiceBatimentDto iServBatimentDto;
	
	@Override
	public Batiment creation(Batiment batiment) {
	
		return iServBatimentDto.creation(batiment);
	}

	@Override
	public Batiment update(Batiment batiment) {
		return iServBatimentDto.update(batiment);
	}

	@Override
	public Batiment get(Integer id) {
		return iServBatimentDto.get(id);
	}

	@Override
	public List<Batiment> getAll() {
		
		return iServBatimentDto.getAll();
	}

	@Override
	public Batiment delete(Integer id) {
		return iServBatimentDto.delete(id);
	}
}
