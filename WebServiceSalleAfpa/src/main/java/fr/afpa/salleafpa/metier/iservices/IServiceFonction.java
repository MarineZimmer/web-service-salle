package fr.afpa.salleafpa.metier.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.Fonction;

public interface IServiceFonction {

	Fonction create(Fonction focntion);

	Fonction update(Fonction role);

	Fonction getFonction(Integer id);

	List<Fonction> getAllFonction();

	Fonction delete(Integer id);


}
