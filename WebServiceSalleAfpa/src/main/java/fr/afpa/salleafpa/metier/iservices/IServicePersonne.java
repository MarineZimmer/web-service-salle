package fr.afpa.salleafpa.metier.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.Personne;

public interface IServicePersonne {
	
	
	public Personne creationPersonneWS(Personne personne);
	
	public Personne updatePersonneWS(Personne personne);
	
	public Personne deletePersonneWS(String login);
	
	/**
	 * Service de suppression d'une personne
	 * 
	 * @param login : le login de la personne à supprimer
	 * @return : true si la suppresion à été effectué, false sinon
	 */
	public boolean deletePersonne(String login);
	
	/**
	 * Service pour le update de la personne
	 * 
	 * @param nom      : nom modifié ou non
	 * @param prenom   : prenom ou non
	 * @param mail     : mail ou non
	 * @param tel      : tel ou non
	 * @param adresse  : adresse ou non
	 * @param date     : date ou non
	 * @param actif    : actif ou non
	 * @param login    : login ou non
	 * @param role     : role ou non
	 * @param fonction
	 * @return un boolean true si la mise à jour à bien ete effectué, false dans le
	 *         cas contraire
	 */
	public boolean updatePers(String nom,String prenom, String mail, String tel, String adresse, String date, String actif, String login, String role, String fonction );
	
	/**
	 * Service de creation d'une personne
	 * 
	 * @param nom             : le nom de la personne
	 * @param prenom          : le prenom de la personne
	 * @param mail            : le mail de la personne
	 * @param tel             : le téléphone de la personne
	 * @param adresse         : l'adresse de la personne
	 * @param dateDeNaissance : la date de naissance de la personne
	 * @param idRole          : le role de la personne
	 * @param idFonction      : la fonction de la personne
	 * @param login           : le login de la personne
	 * @param mdp             : le mot de passe de la personne
	 * @param actif           :
	 * @return : true si la personne est bien crée, false sinon
	 */
	public boolean creationPersonne(String nom , String prenom, String mail, String tel ,String adresse, String dateDeNaissance , String idRole, String idFonction ,String login, String mdp,boolean actif);
	
	
	
	
	
	/**
	 * Service qui renvoie la personne correspondant au login en parametre 
	 * @param Login le login recherché
	 * @return une entite metier personne correspondant au login
	 */
	public Personne getPersonneByLogin(String login);
	
	/**
	 * Service qui renvoie la liste de tous les personnnes
	 * @return une liste d'entité métier personne,  correspondant à tous les personnes
	 */
	public List<Personne> getAllPersonnes();
}
