package fr.afpa.salleafpa.metier.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.TypeMateriel;

public interface IServiceTypeMateriel{

	TypeMateriel creationTypeMateriel(TypeMateriel typeMateriel);

	TypeMateriel updateTypeMateriel(TypeMateriel typeMateriel);

	TypeMateriel getTypeMateriel(Integer id);

	List<TypeMateriel> getAllTypeMateriel();

	TypeMateriel deleteTypeMateriel(Integer id);
}

