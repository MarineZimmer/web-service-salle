package fr.afpa.salleafpa.metier.iservices;

import java.time.LocalDate;
import java.util.List;

import fr.afpa.salleafpa.metier.entities.Salle;

public interface IServiceFiltreSalle {

	/**
	 * Service metier de filtre selon la disponibilié des salles pendant une période
	 * @param listeSalle : la liste des salles à filtrer
	 * @param dateDebut : la date de début
	 * @param dateFin : la date fin
	 * @param libre : true si on souhaite connaitre les salles libre, false si on veut garder les salle occuppés
	 * @return la liste des  salles filtrés selon leur disponibilité ou non durant une période
	 */
	public List<Salle> filtreDateSalle(List<Salle> listeSalle, LocalDate dateDebut,LocalDate dateFin, boolean libre);
	
	/**
	 *  Service metier de filtre selon le type de la salle
	 * @param listeSalle :  la liste des salles à filtrer
	 * @param listeId : la liste des identifiant des types de salle que l'on souhaite garder
	 * @return : une liste de salle correspondant au type de salles recherché
	 */
	public List<Salle> filtreTypeSalle(List<Salle> listeSalle, List<Integer> listeId);

	/**
	 * Service metier de filtre selon la quantité de matériel diponible dans la salle
	 * @param listeSalle :  la liste des salles à filtrer
	 * @param id : l'identifiant du matériel que la salle doit avoir
	 * @param quantite : la quantité de matériel que la salle doit avoir au minimum
	 * @return :  une liste de salle possédant le materiel recherché avec une quantité minimum
	 */
	public List<Salle> filtreMateriel(List<Salle> listeSalle, int id, int quantite);
	
	/**
	 * Service metier de filtre selon la capacité d'acceuil de la salle
	 * 
	 * @param listeSalle : la liste des salles à filtrer
	 * @param min:       la capacité minimum de la salle
	 * @return : une liste de salle pouvant acceuillir au moins n personnes
	 */
	public List<Salle> filtreCapacite(List<Salle> listeSalle, int min);
	
	/**
	 * service qui vérifie si la salle est réservée ou libre durant une période
	 * @param salle : la salle à vérifier
	 * @param dateDebut : la date de début de la période
	 * @param dateFin : la date de fin de la période
	 * @param libre : true si on veux savoir si la salle est libre, false si on souhaite savoir si la salle est occuppée
	 * @return true si la salle correspond au filtre demandé
	 */
	public boolean etatSalle(Salle salle, LocalDate dateDebut, LocalDate dateFin, boolean libre );
	
	/**
	 * Service metier de filtre selon si la salle est actif ou non
	 * 
	 * @param listeSalle : la liste des salles à filtrer
	 * @param actif:       true si on veut les salles actifs, false si on veut les salles inactifs
	 * @return : une liste de salle pouvant acceuillir au moins n personnes
	 */
	public List<Salle> filtreActif(List<Salle> listeSalle, boolean actif);
}
