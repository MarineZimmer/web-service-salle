package fr.afpa.salleafpa.metier.services;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dao.entities.PersonneDao;
import fr.afpa.salleafpa.dto.iservices.IServicePersonnenDto;
import fr.afpa.salleafpa.dto.services.ServicePersonneDto;
import fr.afpa.salleafpa.metier.entities.Authentification;
import fr.afpa.salleafpa.metier.entities.Fonction;
import fr.afpa.salleafpa.metier.entities.Personne;
import fr.afpa.salleafpa.metier.entities.Role;
import fr.afpa.salleafpa.metier.iservices.IServicePersonne;

@Service
public class ServicePersonne implements IServicePersonne {

	@Autowired
	private IServicePersonnenDto iServPersDto;

	@Override
	public Personne deletePersonneWS(String login) {
		return iServPersDto.deletePersonneWS(login);
	}
	
	
	
	@Override
	public Personne creationPersonneWS(Personne personne) {

		return iServPersDto.savePersonneWS(personne);

	}
	
	
	@Override
	public Personne updatePersonneWS(Personne personne) {
		
		return iServPersDto.updatePersonneWS(personne);

	}

	@Override
	public boolean updatePers(String nom, String prenom, String mail, String tel, String adresse, String date,
			String actif, String login, String role, String fonction) {

		Personne personne = iServPersDto.getPersonneByLogin(login);

		LocalDate date2 = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"));

		personne.setNom(nom);
		personne.setPrenom(prenom);
		personne.setMail(mail);
		personne.setTel(tel);
		personne.setAdresse(adresse);
		personne.setDateDeNaissance(date2);
		if (actif.matches("true")) {
			personne.setActif(true);
		} else if (actif.matches("false")) {
			personne.setActif(false);
		}
		personne.getRole().setId(Integer.parseInt(role));
		personne.getFonction().setId(Integer.parseInt(fonction));

		return iServPersDto.updatePersonne(personne);

	}
	
	@Override
	public boolean deletePersonne(String login) {
		Personne personne = iServPersDto.getPersonneByLogin(login);
		return iServPersDto.deletePersonne(personne);
	}

	@Override
	public boolean creationPersonne(String nom, String prenom, String mail, String tel, String adresse,
			String dateDeNaissance, String idRole, String idFonction, String login, String mdp, boolean actif) {

		Role role = new Role();
		role.setId(Integer.parseInt(idRole));
		Fonction fonction = new Fonction();
		fonction.setId(Integer.parseInt(idFonction));
		Authentification authentification = new Authentification();
		authentification.setLogin(login);
		authentification.setMdp(mdp);
		Personne personne = new Personne();
		personne.setNom(nom);
		personne.setPrenom(prenom);
		personne.setMail(mail);
		personne.setTel(tel);
		personne.setAdresse(adresse);
		personne.setDateDeNaissance(LocalDate.parse(dateDeNaissance));
		personne.setRole(role);
		personne.setFonction(fonction);
		personne.setAuthentification(authentification);
		personne.setActif(actif);
		personne.setId(0);
		return iServPersDto.savePersonne(personne);

	}
	
	
	

	@Override
	public Personne getPersonneByLogin(String login) {

		return iServPersDto.getPersonneByLogin(login);
	}
	
	
	
	@Override
	public List<Personne> getAllPersonnes() {
		return iServPersDto.getAllPersonnes();
		
	}
	
	
	
}
