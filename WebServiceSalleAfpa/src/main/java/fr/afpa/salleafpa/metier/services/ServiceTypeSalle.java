package fr.afpa.salleafpa.metier.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dto.iservices.IServiceTypeSalleDto;
import fr.afpa.salleafpa.metier.entities.Batiment;
import fr.afpa.salleafpa.metier.entities.TypeSalle;
import fr.afpa.salleafpa.metier.iservices.IServiceTypeSalle;

@Service
public class ServiceTypeSalle implements IServiceTypeSalle {
	@Autowired
	private IServiceTypeSalleDto iServTypeSalleDto;
	
	@Override
	public TypeSalle creation(TypeSalle typeSalle) {
	
		return iServTypeSalleDto.creation(typeSalle);
	}

	@Override
	public TypeSalle update(TypeSalle typeSalle) {
		return iServTypeSalleDto.update(typeSalle);
	}

	@Override
	public TypeSalle get(Integer id) {
		return iServTypeSalleDto.get(id);
	}

	@Override
	public List<TypeSalle> getAll() {
		
		return iServTypeSalleDto.getAll();
	}

	@Override
	public TypeSalle delete(Integer id) {
		return iServTypeSalleDto.delete(id);
	}
	


}
