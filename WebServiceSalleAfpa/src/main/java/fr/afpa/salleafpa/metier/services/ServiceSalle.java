package fr.afpa.salleafpa.metier.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.salleafpa.dto.iservices.IServiceSalleDto;
import fr.afpa.salleafpa.metier.entities.Salle;
import fr.afpa.salleafpa.metier.iservices.IServiceSalle;

@Service
public class ServiceSalle implements IServiceSalle{
	
	@Autowired
	private IServiceSalleDto servSalleDto;
	
	@Override
	public Salle createSalle(Salle salle) {
		return servSalleDto.createSalle(salle);
		
	}
	
	
	@Override
	public List<Salle> getAllSalles() {
		return servSalleDto.getAllSalles();
		
	}
	
	/**
	 * Methode pour recup une salle via appel au service dto
	 */
	@Override
	public Salle getSalle(int id) {
		return servSalleDto.getSalle(id);
		
	}


	@Override
	public boolean updateSalle(Salle salle) {
		return servSalleDto.updateSalle(salle);
	}

}
