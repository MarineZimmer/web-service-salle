package fr.afpa.salleafpa.metier.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.Salle;


public interface IServiceSalle {
	/**
	 * Service metier pour enregistrer une salle dans la base de données
	 * @param salle : la nouvelle salle à enregistrer
	 * @return : la salle enregistré
	 */
	public Salle createSalle(Salle salle);
	/**
	 * Service métier pour retourner une liste de salles dans la base de données
	 * @return : la liste de toutes les salles
	 */
	public List<Salle> getAllSalles();
	
	/**
	 * Service metier pour retourner un seule salle via son identifiant
	 * @param id : identifiant de la salle selectionnée
	 * @return : la salle crée
	 */
	public Salle getSalle(int id);
	
	/**
	 * Service pour modifier une salle qu'on recupéré via son identifiant;
	 * @param salle: la salle modifié
	 * @return : vrai si la salle à bien été modifié, false dans le cas contraire
	 */
	public boolean updateSalle(Salle salle);
}
