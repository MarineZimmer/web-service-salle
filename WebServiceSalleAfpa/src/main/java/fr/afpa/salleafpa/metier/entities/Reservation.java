package fr.afpa.salleafpa.metier.entities;

import java.time.LocalDate;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Reservation {
	
	


	Integer id;
	
	
	LocalDate dateDebut;
	
	LocalDate dateFin;
	
	String nomReservation;
	
	
	Salle salle;//????


	public Reservation(int idReservation, LocalDate dateDebut, LocalDate dateFin, String nomReservation) {
		super();
		this.id = idReservation;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.nomReservation = nomReservation;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Reservation) {
		return this.getId()==((Reservation)obj).getId();
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.getId();
	}
	
	

}
