package fr.afpa.salleafpa.outils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ControleSaisie {

	private ControleSaisie() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * Controlle si le numero est valide
	 * @param numero
	 * @returntrue si valide, faux sinon
	 */
	public static boolean isChiffre(String numero) {
		try {
			Integer.parseInt(numero);
			return true;
			
		} catch (Exception e) {
			Logger.getLogger(ControleSaisie.class.getName()).log(Level.SEVERE, null, e);
			return false;
		}
		
	}
	/**
	 * Comtrole si le nom ou le prenom est valide
	 * 
	 * @param nom : le nom a controler
	 * @return true si le nom est valide
	 */
	public static boolean isNom(String nom) {
		return nom.matches("^[A-Za-z][A-Za-z- ]*$");
	}

	/**
	 * Controle si le mail est vailde
	 * 
	 * @param mail mail a controler
	 * @return true si le mail est valide, false sinon
	 */
	public static boolean isMail(String mail) {
		String regexMailPartie1 = "\\w(\\.?[\\w-]+)*";
		String regexMailPartie2 = "[\\w&&[^_]]{3,13}";
		String regexMailPartie3 = "[\\w&&[^_]]{2,3}";
		String rejexMail = "^" + regexMailPartie1 + "@" + regexMailPartie2 + "\\." + regexMailPartie3 + "$";
		return mail.matches(rejexMail);
	}

	/**
	 * controle si la date de naissance est valide
	 * 
	 * @param date date à controller
	 * @return true si la chaine est une date valide, false sinon
	 */
	public static boolean isDateValide(String date) {

		try {
			LocalDate date2 = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			if (date2.isBefore(LocalDate.now().minusYears(16)) && date2.isAfter(LocalDate.now().minusYears(81))) {
				return true;
			}
		} catch (Exception e) {
			Logger.getLogger(ControleSaisie.class.getName()).log(Level.SEVERE, null, e);
			return false;
		}
		return false;

	}
	/**
	 * controle si la date de naissance est valide
	 * 
	 * @param date date à controller
	 * @return true si la chaine est une date valide, false sinon
	 */
	public static boolean isDateValideWS(LocalDate date) {

		try {
			
			if (date.isBefore(LocalDate.now().minusYears(16)) && date.isAfter(LocalDate.now().minusYears(81))) {
				return true;
			}
		} catch (Exception e) {
			Logger.getLogger(ControleSaisie.class.getName()).log(Level.SEVERE, null, e);
			return false;
		}
		return false;

	}

	/**
	 * controle si le telephone est valide (10 chiffres sans espace)
	 * 
	 * @param tel le numero à controller
	 * @return true si numero est valide, false sinon
	 */
	public static boolean isNumTel(String tel) {

		return tel.matches("\\d{10}");
	}

	/**
	 * methode qui verifie si la chaine de caractere et non vide (les espaces sont
	 * concidéré comme vide)
	 * 
	 * @param chaine chaine de caractère à vérifier
	 * @return true si la chaine de caractère est non vide et n'est pas constitué
	 *         que des espaces, false sinon
	 */
	public static boolean isNonVide(String chaine) {
		return chaine.replaceAll(" ", "").length() > 0;

	}

	/**
	 * Controle si les dates de réservation sont valides
	 * 
	 * @param dateDebut : la date de début de la réservation
	 * @param dateFin   : la date de fin de la réservation
	 * @return true si les dates de réservation sont correctes
	 */
	public static boolean isDateReservation(String dateDebut, String dateFin) {
		try {
			LocalDate debut = LocalDate.parse(dateDebut, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			LocalDate fin = LocalDate.parse(dateFin, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			if (fin.compareTo(debut) >= 0 && debut.compareTo(LocalDate.now()) >= 0) {
				return true;
			}
		} catch (Exception e) {
			Logger.getLogger(ControleSaisie.class.getName()).log(Level.SEVERE, null, e);
			return false;
		}
		return false;
	}

	/**
	 * Controle si le chifre entrée est positif
	 * @param id : chiffre passé en parametres
	 * @return : vrai si le chiffre est positif, faux dans le cas contraire
	 */
	public static boolean isIntegerPositif(String id) {
		try {
			if (Integer.parseInt(id) >= 0) {
				return true;
			}
		} catch (Exception e) {
			Logger.getLogger(ControleSaisie.class.getName()).log(Level.SEVERE, null, e);
			return false;
		}
		return false;
	}

	/**
	 * Controle si les dates de réservation pour la modification sont valides
	 * 
	 * @param dateDebut   : la nouvelle date de début de la réservation
	 * @param dateFin     : la nouvelle date de fin de la réservation
	 * @param debutReserv : l'ancienne date de début
	 * @return true si les dates de réservation sont correctes
	 */
	public static boolean isDateReservationModification(String dateDebut, String dateFin, LocalDate debutReserv) {
		try {
			LocalDate debut = LocalDate.parse(dateDebut, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			LocalDate fin = LocalDate.parse(dateFin, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			if (fin.compareTo(debut) >= 0 && debut.compareTo(LocalDate.now()) >= 0 && !debutReserv.equals(debut)) {
				return true;
			}
			if (debutReserv.equals(debut) && fin.compareTo(LocalDate.now()) >= 0 && fin.compareTo(debut) >= 0) {
				return true;
			}

		} catch (Exception e) {
			Logger.getLogger(ControleSaisie.class.getName()).log(Level.SEVERE, null, e);
			return false;
		}
		return false;
	}

}
