package fr.afpa.salleafpa.controlleurs;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import fr.afpa.salleafpa.metier.entities.Batiment;
import fr.afpa.salleafpa.metier.entities.Materiel;
import fr.afpa.salleafpa.metier.entities.Personne;
import fr.afpa.salleafpa.metier.entities.Reservation;
import fr.afpa.salleafpa.metier.entities.Salle;
import fr.afpa.salleafpa.metier.entities.TypeMateriel;
import fr.afpa.salleafpa.metier.entities.TypeSalle;
import fr.afpa.salleafpa.metier.iservices.IServiceBatiment;
import fr.afpa.salleafpa.metier.iservices.IServiceFiltreSalle;
import fr.afpa.salleafpa.metier.iservices.IServiceSalle;
import fr.afpa.salleafpa.metier.iservices.IServiceTypeMateriel;
import fr.afpa.salleafpa.metier.iservices.IServiceTypeSalle;
import fr.afpa.salleafpa.outils.ControleSaisie;
import fr.afpa.salleafpa.outils.Parametrage;

/**
 * Handles requests for the application home page.
 */
@Controller
public class SalleController {

	private static final Logger logger = LoggerFactory.getLogger(SalleController.class);

	@Autowired
	private IServiceSalle servSalle;

	@Autowired
	private IServiceTypeMateriel servTypeMateriel;

	@Autowired
	private IServiceTypeSalle servTypeSalle;

	@Autowired
	private IServiceBatiment servBatiment;

	@Autowired
	private IServiceFiltreSalle servFiltreSalle;

	/**
	 * Methode pour renvoyer vers la page de creation d'une salle
	 * 
	 * @param mv      : model and view
	 * @param session : session en cours avec la personne authentifié
	 * @return : le model and view initialisé, si la personne n'est pas authentifié
	 *         et n'est pas admin redirection vers la page de connexion
	 */
	@GetMapping(value = "/creation")
	public ModelAndView ajouterSalleGet(ModelAndView mv, HttpSession session) {
		if (session.getAttribute("persAuthSalle") instanceof Personne
				&& ((Personne) session.getAttribute("persAuthSalle")).getRole().getId() == Parametrage.ADMIN_ID) {
			mv.setViewName("ajoutSalle");

			List<TypeSalle> listeTypeSalle = servTypeSalle.getAll();
			List<Batiment> listeBatiment = servBatiment.getAll();
			List<TypeMateriel> listeMateriel = servTypeMateriel.getAllTypeMateriel();

			mv.addObject("listeTypeSalle", listeTypeSalle);
			mv.addObject("listeBatiment", listeBatiment);
			mv.addObject("listeTypeMateriel", listeMateriel);
		} else {
			RedirectView redirectView = new RedirectView(Parametrage.URI_AUTHENTIFICATION);
			mv.setView(redirectView);
		}
		return mv;
	}

	/**
	 * Methode pour lancer la creation de la salle
	 * 
	 * @param mv      : model and view
	 * @param session : session en cours avec la personne authentifié
	 * @return : le model and view initialisé
	 */
	@PostMapping(value = "/creation")
	public ModelAndView ajouterSallePost(ModelAndView mv, HttpSession session,
			@RequestParam(value = "nomSalle") String nom, @RequestParam(value = "typeSalle") String typeSalle,
			@RequestParam(value = "bat") String batiment, @RequestParam(value = "capacite") String capacite,
			@RequestParam(value = "numeroSalle") String num, @RequestParam(value = "etage") String etage,
			@RequestParam(value = "superficie") String superficie, @RequestParam(value = "actif") String actif,
			@RequestParam Map<String, String> params) {

		if (session.getAttribute("persAuthSalle") instanceof Personne
				&& ((Personne) session.getAttribute("persAuthSalle")).getRole().getId() == Parametrage.ADMIN_ID) {
			boolean ok = true;
			boolean activerSalle = true;

			if (!ControleSaisie.isNonVide(nom)) {
				ok = false;
				mv.addObject("nomKO", "*Nom de la salle vide");

			}
			if (!ControleSaisie.isIntegerPositif(num)) {
				ok = false;
				mv.addObject("numeroKO", "*Doit être un chiffre positif");
			}
			if (!ControleSaisie.isIntegerPositif(superficie)) {
				ok = false;
				mv.addObject("superficieKO", "*Erreur de saisie: doit être un chiffre positif");
			}
			if (!ControleSaisie.isIntegerPositif(capacite)) {
				ok = false;
				mv.addObject("capaciteKO", "*Erreur de saisie: doit être un chiffre positif");
			}

			if (!ControleSaisie.isIntegerPositif(capacite)) {
				ok = false;
				mv.addObject("capaciteKO", "*Erreur de saisie: doit être un chiffre positif");
			}

			if (!ControleSaisie.isChiffre(etage)) {
				ok = false;
				mv.addObject("etageKO", "*Erreur de saisie: doit être un chiffre");
			}

			// set du boolean actif ou non
			if ("false".equals(actif)) {
				activerSalle = false;
			}

			if (ok) {
				// creation salle
				Salle salle = new Salle(0, Integer.parseInt(num), nom, Float.parseFloat(superficie),
						Integer.parseInt(capacite), Integer.parseInt(etage), activerSalle);

				// set le Type de Salle en fonction du type de Salle selectionne
				salle.setTypeSalle(servTypeSalle.get(Integer.parseInt(typeSalle)));

				// set le Type de Salle en fonction du Batiment selectionne
				salle.setBatiment(servBatiment.get(Integer.parseInt(batiment)));

				// set de la liste des reservation pour pas avoir de null pointeurs
				salle.setListeReservation(new ArrayList<Reservation>());

				// creation de la liste de MAteriel pour la salle
				List<Materiel> listeMateriel = new ArrayList<Materiel>();

				// init la liste de materiel
				for (Entry<String, String> param : params.entrySet()) {

					if ("tm".equals(param.getKey().substring(0, 2)) && !"".equals(param.getValue())) {

						Materiel materiel = new Materiel(0, salle,
								new TypeMateriel(Integer.parseInt(param.getKey().substring(2)), ""),
								Integer.parseInt(param.getValue()));
						listeMateriel.add(materiel);
					}
				}

				salle.setListeMateriel(listeMateriel);

				// enregistrement salle
				servSalle.createSalle(salle);

				// redirection vers la page avec toutes les listes de salles
				RedirectView redirectView = new RedirectView(Parametrage.URI_VISUALISER_SALLE);
				mv.setView(redirectView);
			} else {
				// si erreur retour vers la page de creation
				return ajouterSalleGet(mv, session);
			}
		} else {
			// si personne pas authentifié retour vers la page d'authentification
			RedirectView redirectView = new RedirectView(Parametrage.URI_VISUALISER_SALLE);
			mv.setView(redirectView);
		}

		return mv;
	}

	/**
	 * Methode pour rediriger vers la page qui va afficher les salles
	 * 
	 * @param mv      : ModelandView
	 * @param session : Session pour les verification de la personne authentifié
	 * @return : le modelAndView crée.
	 */
	@RequestMapping(value = "/visualiser", method = RequestMethod.GET)
	public ModelAndView visualisationsalleGet(ModelAndView mv, HttpSession session) {

		// verifie si une personne est bien authentifié pour accéder au menu
		if (session.getAttribute("persAuthSalle") instanceof Personne) {
			mv.setViewName("visualisationSalle");

			if (!mv.getModel().containsKey("listeSalle")) {
				List<Salle> listeSalle = servSalle.getAllSalles();
				mv.addObject("listeSalle", listeSalle);
			}

			List<TypeSalle> listeTypeSalle = servTypeSalle.getAll();
			mv.addObject("listeTypeSalle", listeTypeSalle);

			List<TypeMateriel> listeTypeMateriel = servTypeMateriel.getAllTypeMateriel();
			mv.addObject("listeTypeMateriel", listeTypeMateriel);

		} else { // non authentifier retour à la page authentification
			RedirectView redirectView = new RedirectView(Parametrage.URI_AUTHENTIFICATION);
			mv.setView(redirectView);
		}

		return mv;

	}

	/**
	 * Mapping vers la page de la salle selectionné dans la page precedente
	 * 
	 * @param mv      :model and view qui sera envoyé vers la jsp defini
	 * @param session : pour la verification de la personne authentifée
	 * @param id      : l'identifiant de la salle envoyé via parametres à partir de
	 *                la page precedente
	 * @return : ModelAndView
	 */
	@RequestMapping(value = "/visualiser/salle", method = RequestMethod.GET)
	public ModelAndView visualisationUneSalleGet(ModelAndView mv, HttpSession session,
			@RequestParam(value = "id") String id) {

		// verification si une personne est bien authentifié pour accéder à la page
		// demandé
		if (session.getAttribute("persAuthSalle") instanceof Personne) {
			mv.setViewName("visualisationUneSalle");

			Salle salle = servSalle.getSalle(Integer.parseInt(id));
			mv.addObject("salle", salle);

		} else { // si pas authentifié alors retour à la page d'authentification

			RedirectView redirectView = new RedirectView(Parametrage.URI_AUTHENTIFICATION);
			mv.setView(redirectView);
		}

		return mv;

	}
	/**
	 * Mapping vers la page de modification de la salle selectionné dans la page precedente
	 * 
	 * @param mv      :model and view qui sera envoyé vers la jsp defini
	 * @param session : pour la verification de la personne authentifée
	 * @param id      : l'identifiant de la salle envoyé via parametres à partir de
	 *                la page precedente
	 * @return : ModelAndView
	 */
	@RequestMapping(value = "/visualiser/salle/modifier", method = RequestMethod.GET)
	public ModelAndView modifierUneSalleGet(ModelAndView mv, HttpSession session,
			@RequestParam(value = "id") String id) {
		

		// verification si une personne est bien authentifié pour accéder à la page
		// demandé
		if (session.getAttribute("persAuthSalle") instanceof Personne
				&& ((Personne) session.getAttribute("persAuthSalle")).getRole().getId() == Parametrage.ADMIN_ID) {
			mv.setViewName("modifierUneSalle");

			//recuperation de la salle via son identifiant
			Salle salle = servSalle.getSalle(Integer.parseInt(id));
			mv.addObject("salle", salle);
			
			//init de la liste des types de salles disponibles
			List<TypeSalle> listeTypeSalle = servTypeSalle.getAll();
			mv.addObject("listeTypeSalle", listeTypeSalle);
			
			//init de la liste de Materiel Disponible
			List<TypeMateriel> listeTypeMateriel = servTypeMateriel.getAllTypeMateriel();
			mv.addObject("listeTypeMateriel", listeTypeMateriel);

			

		} else { // si pas authentifié alors retour à la page d'authentification

			RedirectView redirectView = new RedirectView(Parametrage.URI_AUTHENTIFICATION);
			mv.setView(redirectView);
		}

		return mv;

	}
	
	/**
	 * Mapping vers la page de modification de la salle selectionné dans la page precedente
	 * 
	 * @param mv      :model and view qui sera envoyé vers la jsp defini
	 * @param session : pour la verification de la personne authentifée
	 * @param id      : l'identifiant de la salle envoyé via parametres à partir de
	 *                la page precedente
	 * @return : ModelAndView
	 */
	@RequestMapping(value = "/visualiser/salle/modifier", method = RequestMethod.POST)
	public ModelAndView modifierUneSalle(ModelAndView mv, HttpSession session,
			@RequestParam(value = "nomSalle") String nom, @RequestParam(value = "typeSalle") String typeSalle,
			@RequestParam(value = "capacite") String capacite, @RequestParam(value = "actif") String actif,
			@RequestParam(value="id") String id,@RequestParam(value = "numeroSalle") String num,
			@RequestParam Map<String, String> params) {
		
			boolean ok=true;
			boolean activerSalle=true;
		// verification si une personne est bien authentifié pour accéder à la methode 
		if (session.getAttribute("persAuthSalle") instanceof Personne
				&& ((Personne) session.getAttribute("persAuthSalle")).getRole().getId() == Parametrage.ADMIN_ID) {
			
			if (!ControleSaisie.isNonVide(nom)) {
				ok = false;
				mv.addObject("nomKO", "*Nom de la salle vide");

			}
			if (!ControleSaisie.isIntegerPositif(num)) {
				ok = false;
				mv.addObject("numeroKO", "*Doit être un chiffre positif");
			}
			
			if (!ControleSaisie.isIntegerPositif(capacite)) {
				ok = false;
				mv.addObject("capaciteKO", "*Erreur de saisie: doit être un chiffre positif");
			}

			if (!ControleSaisie.isIntegerPositif(capacite)) {
				ok = false;
				mv.addObject("capaciteKO", "*Erreur de saisie: doit être un chiffre positif");
			}

			

			// set du boolean actif ou non
			if ("false".equals(actif)) {
				activerSalle = false;
			}
			
			//recuperation de la salle via son identifiant
			Salle salle = servSalle.getSalle(Integer.parseInt(id));
			List<Materiel>listeMateriel=new ArrayList<Materiel>();
			
			salle.getListeReservation().forEach(s->s.setSalle(salle));
			if(ok) {
				salle.setNom(nom);
				salle.setActif(activerSalle);
				salle.setNumero(Integer.parseInt(num));
				salle.setCapacite(Integer.parseInt(capacite));
				//salle.setTypeSalle(typeSalle);
				
				// init la liste de materiel
				for (Entry<String, String> param : params.entrySet()) {

					if ("tm".equals(param.getKey().substring(0, 2)) && !"".equals(param.getValue())) {

						Materiel materiel = new Materiel(0, salle,
								new TypeMateriel(Integer.parseInt(param.getKey().substring(2)), ""),
								Integer.parseInt(param.getValue()));
						listeMateriel.add(materiel);
					}
				}
			
			//reset de la liste des materiaux
			salle.setListeMateriel(listeMateriel);
			
			//update de la salle
			servSalle.updateSalle(salle);
			
			//redirection vers le menu principal
			RedirectView redirectView = new RedirectView(Parametrage.URI_VISUALISER_SALLE);
			mv.setView(redirectView);
			}
			else {
				return modifierUneSalleGet(mv, session,salle.getIdSalle().toString());
			}			

		} else { // si pas authentifié alors retour à la page d'authentification

			RedirectView redirectView = new RedirectView(Parametrage.URI_AUTHENTIFICATION);
			mv.setView(redirectView);
		}

		return mv;

	}
	
	

	/**
	 * Methode pour rediriger vers la page qui va afficher les salles
	 * 
	 * @param mv      : ModelandView
	 * @param session : Session pour les verification de la personne authentifié
	 * @return : le modelAndView crée.
	 */
	@RequestMapping(value = "/visualiser", method = RequestMethod.POST)
	public ModelAndView visualisationsallePost(ModelAndView mv, HttpSession session,
			@RequestParam Map<String, String> params, @RequestParam(value = "typeSalle") String[] tabTypeSalle) {

		mv.setViewName("visualisationSalle");

		List<Salle> listeSalle = servSalle.getAllSalles();

		// filtre actif
		if ("true".equals(params.get("actif"))) {
			listeSalle = servFiltreSalle.filtreActif(listeSalle, true);

		} else if ("false".equals(params.get("actif"))) {
			listeSalle = servFiltreSalle.filtreActif(listeSalle, false);

		}
		// filtre date
		if ("reserve".equals(params.get("date")) && !"".equals(params.get("dateDebut"))
				&& !"".equals(params.get("dateFin"))) {
			listeSalle = servFiltreSalle.filtreDateSalle(listeSalle, LocalDate.parse(params.get("dateDebut")),
					LocalDate.parse(params.get("dateFin")), false);

		} else if ("libre".equals(params.get("date")) && !"".equals(params.get("dateDebut"))
				&& !"".equals(params.get("dateFin"))) {
			listeSalle = servFiltreSalle.filtreDateSalle(listeSalle, LocalDate.parse(params.get("dateDebut")),
					LocalDate.parse(params.get("dateFin")), true);
		}

		// filtre capacite
		if (!"".equals(params.get("capacite"))) {
			listeSalle = servFiltreSalle.filtreCapacite(listeSalle, Integer.parseInt(params.get("capacite")));
		}

		// filtre type salle
		if (tabTypeSalle != null && tabTypeSalle.length > 1) {
			
			List<Integer> listeTypeSalle = Arrays.asList(tabTypeSalle).stream().map(Integer::parseInt)
					.collect(Collectors.toList());
			listeSalle = servFiltreSalle.filtreTypeSalle(listeSalle, listeTypeSalle);
		}

		// filtre materiel
		for (Entry<String, String> param : params.entrySet()) {
			if ("tm".equals(param.getKey().substring(0, 2)) && !"".equals(param.getValue())) {
				listeSalle = servFiltreSalle.filtreMateriel(listeSalle, Integer.parseInt(param.getKey().substring(2)),
						Integer.parseInt(param.getValue()));
			}
		}

		mv.addObject("listeSalle", listeSalle);
		return visualisationsalleGet(mv, session);

	}

}
