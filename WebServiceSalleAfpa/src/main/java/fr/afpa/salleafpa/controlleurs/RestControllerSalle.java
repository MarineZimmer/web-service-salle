package fr.afpa.salleafpa.controlleurs;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import fr.afpa.salleafpa.metier.entities.Batiment;
import fr.afpa.salleafpa.metier.entities.Materiel;
import fr.afpa.salleafpa.metier.entities.Personne;
import fr.afpa.salleafpa.metier.entities.Reservation;
import fr.afpa.salleafpa.metier.entities.Salle;
import fr.afpa.salleafpa.metier.entities.TypeMateriel;
import fr.afpa.salleafpa.metier.entities.TypeSalle;
import fr.afpa.salleafpa.metier.iservices.IServiceBatiment;
import fr.afpa.salleafpa.metier.iservices.IServiceFiltreSalle;
import fr.afpa.salleafpa.metier.iservices.IServiceSalle;
import fr.afpa.salleafpa.metier.iservices.IServiceTypeMateriel;
import fr.afpa.salleafpa.metier.iservices.IServiceTypeSalle;
import fr.afpa.salleafpa.outils.ControleSaisie;
import fr.afpa.salleafpa.outils.Parametrage;

/**
 * Handles requests for the application home page.
 */
@RestController
//@CrossOrigin(origins = "http://127.0.0.1:5500")
@CrossOrigin(origins = "http://localhost:3000")
public class RestControllerSalle {

	private static final Logger logger = LoggerFactory.getLogger(RestControllerSalle.class);

	@Autowired
	private IServiceSalle servSalle;

	@Autowired
	private IServiceTypeMateriel servTypeMateriel;

	@Autowired
	private IServiceTypeSalle servTypeSalle;

	@Autowired
	private IServiceBatiment servBatiment;

	@Autowired
	private IServiceFiltreSalle servFiltreSalle;

	

	/**
	 * Methode pour lancer la creation de la salle
	 * 
	 * @param mv      : model and view
	 * @param session : session en cours avec la personne authentifié
	 * @return : le model and view initialisé
	 */
	@PostMapping(value = "/salle")
	public ResponseEntity<?> ajouterSallePost( @RequestBody Salle salle, HttpStatus status) {

		System.out.println("creationsalle" +salle.getSurface());
		boolean ok = true;
		Object reponse;
		Map<String, String> map  = new HashMap<String, String>(); 

			if (!ControleSaisie.isNonVide(salle.getNom())) {
				ok = false;
				

			}
			if (!ControleSaisie.isIntegerPositif(""+salle.getNumero())) {
				ok = false;
				map.put("numeroKO", "*Doit être un chiffre positif");
			}
			if (!ControleSaisie.isIntegerPositif(""+Math.round(salle.getSurface()))) {
				ok = false;
				map.put("superficieKO", "*Erreur de saisie: doit être un chiffre positif");
			}
			if (!ControleSaisie.isIntegerPositif(""+salle.getCapacite())) {
				ok = false;
				map.put("capaciteKO", "*Erreur de saisie: doit être un chiffre positif");
			}

			

			if (!ControleSaisie.isChiffre(""+salle.getEtage())) {
				ok = false;
				map.put("etageKO", "*Erreur de saisie: doit être un chiffre");
			}

			

			if (ok) {
				status =status.OK;
				reponse = servSalle.createSalle(salle);
			} else {
				status = status.BAD_REQUEST;
				reponse = map;
			}
		

		return (ResponseEntity<?>) ResponseEntity.status(status).body(reponse);
	}

	/**
	 * Methode pour rediriger vers la page qui va afficher les salles
	 * 
	 * @param mv      : ModelandView
	 * @param session : Session pour les verification de la personne authentifié
	 * @return : le modelAndView crée.
	 */
	@GetMapping(value = "/salle")
	public List<Salle> visualisationsalleGet(ModelAndView mv, HttpSession session) {

			return  servSalle.getAllSalles();


	}

	/**
	 * Mapping vers la page de la salle selectionné dans la page precedente
	 * 
	 * @param mv      :model and view qui sera envoyé vers la jsp defini
	 * @param session : pour la verification de la personne authentifée
	 * @param id      : l'identifiant de la salle envoyé via parametres à partir de
	 *                la page precedente
	 * @return : ModelAndView
	 */
	@GetMapping(value = "/salle/{id}")
	public Salle visualisationUneSalleGet(HttpStatus status, @PathVariable("id") String id) {

		
			Salle salle = servSalle.getSalle(Integer.parseInt(id));
			if (salle != null) {
				return salle;
			} else {
				// status=HttpStatus.BAD_REQUEST;
				return null;
			}

		

	}

	
	/**
	 * Mapping vers la page de modification de la salle selectionné dans la page precedente
	 * 
	 * @param mv      :model and view qui sera envoyé vers la jsp defini
	 * @param session : pour la verification de la personne authentifée
	 * @param id      : l'identifiant de la salle envoyé via parametres à partir de
	 *                la page precedente
	 * @return : ModelAndView
	 */
	@PutMapping(value = "/salle")
	public ResponseEntity<?> modifierUneSalle(@RequestBody Salle salle, HttpStatus status) {
		
		boolean ok = true;
		Object reponse;
		Map<String, String> map  = new HashMap<String, String>(); 

			if (!ControleSaisie.isNonVide(salle.getNom())) {
				ok = false;
				

			}
			if (!ControleSaisie.isIntegerPositif(""+salle.getNumero())) {
				ok = false;
				map.put("numeroKO", "*Doit être un chiffre positif");
			}
			if (!ControleSaisie.isIntegerPositif(""+Math.round(salle.getSurface()))) {
				ok = false;
				map.put("superficieKO", "*Erreur de saisie: doit être un chiffre positif");
			}
			if (!ControleSaisie.isIntegerPositif(""+salle.getCapacite())) {
				ok = false;
				map.put("capaciteKO", "*Erreur de saisie: doit être un chiffre positif");
			}

			

			if (!ControleSaisie.isChiffre(""+salle.getEtage())) {
				ok = false;
				map.put("etageKO", "*Erreur de saisie: doit être un chiffre");
			}

			

			if (ok) {
				status =status.OK;
				reponse = servSalle.updateSalle(salle);
			} else {
				status = status.BAD_REQUEST;
				reponse = map;
			}
		

		return (ResponseEntity<?>) ResponseEntity.status(status).body(reponse);

	}
	
	

	/**
	 * Methode pour rediriger vers la page qui va afficher les salles
	 * 
	 * @param mv      : ModelandView
	 * @param session : Session pour les verification de la personne authentifié
	 * @return : le modelAndView crée.
	 */
	/*@RequestMapping(value = "/visualiser", method = RequestMethod.POST)
	public ModelAndView visualisationsallePost(ModelAndView mv, HttpSession session,
			@RequestParam Map<String, String> params, @RequestParam(value = "typeSalle") String[] tabTypeSalle) {

		mv.setViewName("visualisationSalle");

		List<Salle> listeSalle = servSalle.getAllSalles();

		// filtre actif
		if ("true".equals(params.get("actif"))) {
			listeSalle = servFiltreSalle.filtreActif(listeSalle, true);

		} else if ("false".equals(params.get("actif"))) {
			listeSalle = servFiltreSalle.filtreActif(listeSalle, false);

		}
		// filtre date
		if ("reserve".equals(params.get("date")) && !"".equals(params.get("dateDebut"))
				&& !"".equals(params.get("dateFin"))) {
			listeSalle = servFiltreSalle.filtreDateSalle(listeSalle, LocalDate.parse(params.get("dateDebut")),
					LocalDate.parse(params.get("dateFin")), false);

		} else if ("libre".equals(params.get("date")) && !"".equals(params.get("dateDebut"))
				&& !"".equals(params.get("dateFin"))) {
			listeSalle = servFiltreSalle.filtreDateSalle(listeSalle, LocalDate.parse(params.get("dateDebut")),
					LocalDate.parse(params.get("dateFin")), true);
		}

		// filtre capacite
		if (!"".equals(params.get("capacite"))) {
			listeSalle = servFiltreSalle.filtreCapacite(listeSalle, Integer.parseInt(params.get("capacite")));
		}

		// filtre type salle
		if (tabTypeSalle != null && tabTypeSalle.length > 1) {
			
			List<Integer> listeTypeSalle = Arrays.asList(tabTypeSalle).stream().map(Integer::parseInt)
					.collect(Collectors.toList());
			listeSalle = servFiltreSalle.filtreTypeSalle(listeSalle, listeTypeSalle);
		}

		// filtre materiel
		for (Entry<String, String> param : params.entrySet()) {
			if ("tm".equals(param.getKey().substring(0, 2)) && !"".equals(param.getValue())) {
				listeSalle = servFiltreSalle.filtreMateriel(listeSalle, Integer.parseInt(param.getKey().substring(2)),
						Integer.parseInt(param.getValue()));
			}
		}

		mv.addObject("listeSalle", listeSalle);
		return visualisationsalleGet(mv, session);

	}*/

}
