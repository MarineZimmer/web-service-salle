package fr.afpa.salleafpa.controlleurs;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.salleafpa.metier.entities.TypeSalle;
import fr.afpa.salleafpa.metier.iservices.IServiceTypeSalle;

@RestController

@CrossOrigin(origins = "http://localhost:3000")
public class RestControllerTypeSalle {

	private static final Logger logger = LoggerFactory.getLogger(RestControllerTypeSalle.class);

	@Autowired
	private IServiceTypeSalle servTypeSalle;

	@PostMapping(value = "/typesalle")
	public TypeSalle creationTypeSalleWS(@RequestBody TypeSalle typeSalle) {
		return servTypeSalle.creation(typeSalle);

	}

	@PutMapping(value = "/typesalle")
	
	public TypeSalle updateTypeSalleWS(@RequestBody TypeSalle typeSalle) {

		return servTypeSalle.update(typeSalle);

	}

	@GetMapping(value = "/typesalle/{id}")
	public TypeSalle getTypeSalleWS(@PathVariable("id") Integer id) {
		return servTypeSalle.get(id);
	
	}

	@GetMapping(value = "/typesalle")
	public List<TypeSalle> getListeTypeSallesWS() {
		return servTypeSalle.getAll();

	}

	@DeleteMapping(value = "/typesalle/{id}")
	public TypeSalle deleteTypeSalleWS( @PathVariable("id") Integer id) {
		return servTypeSalle.delete(id);


	}

}
