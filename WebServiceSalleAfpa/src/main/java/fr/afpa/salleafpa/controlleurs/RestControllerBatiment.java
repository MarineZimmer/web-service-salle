package fr.afpa.salleafpa.controlleurs;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.salleafpa.metier.entities.Batiment;
import fr.afpa.salleafpa.metier.entities.Role;
import fr.afpa.salleafpa.metier.iservices.IServiceBatiment;

@RestController

@CrossOrigin(origins = "http://localhost:3000")
public class RestControllerBatiment {

	private static final Logger logger = LoggerFactory.getLogger(RestControllerBatiment.class);

	@Autowired
	private IServiceBatiment servBatiment;

	@PostMapping(value = "/batiment")
	public Batiment creationBatimentWS(@RequestBody Batiment batiment) {
		return servBatiment.creation(batiment);

	}

	@PutMapping(value = "/batiment")
	
	public Batiment updateBatimentWS(@RequestBody Batiment batiment) {

		return servBatiment.update(batiment);

	}

	@GetMapping(value = "/batiment/{id}")
	public Batiment getBatimentWS(@PathVariable("id") Integer id) {
		return servBatiment.get(id);
	
	}

	@GetMapping(value = "/batiment")
	public List<Batiment> getListeBatimentsWS() {
		return servBatiment.getAll();

	}

	@DeleteMapping(value = "/batiment/{id}")
	public Batiment deleteBatimentWS( @PathVariable("id") Integer id) {
		return servBatiment.delete(id);


	}

}
