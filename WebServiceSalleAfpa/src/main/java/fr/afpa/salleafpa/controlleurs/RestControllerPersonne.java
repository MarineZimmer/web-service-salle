package fr.afpa.salleafpa.controlleurs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.function.EntityResponse;

import fr.afpa.salleafpa.metier.entities.Personne;
import fr.afpa.salleafpa.metier.iservices.IServicePersonne;
import fr.afpa.salleafpa.outils.ControleSaisie;

@RestController
//@CrossOrigin(origins = "http://127.0.0.1:5500")
@CrossOrigin(origins = "http://localhost:3000")
public class RestControllerPersonne {

	private static final Logger logger = LoggerFactory.getLogger(RestControllerPersonne.class);

	@Autowired
	private IServicePersonne servPers;

	@PostMapping(value = "/personne/new")
	public ResponseEntity<?> creationPersooneWS(@RequestBody Personne personne, HttpStatus status) {

		boolean ok = true;
		Object reponse;
		Map<String, String> map  = new HashMap<String, String>(); 

		if (!ControleSaisie.isMail(personne.getMail())) {
			ok = false;
			// mv.addObject("mailKO", "*mail invalide");
			map.put("mailKO", "*mail invalide");
		}
		if (!ControleSaisie.isNumTel(personne.getTel())) {
			ok = false;
			// mv.addObject("telKO", "*10 chiffres sans espace");
			map.put("telKO", "*10 chiffres sans espace");
		}
		if (!ControleSaisie.isNonVide(personne.getAuthentification().getLogin())
				|| servPers.getPersonneByLogin(personne.getAuthentification().getLogin()) != null) {
			ok = false;
			// mv.addObject("loginKO", "*login déjà existant ou non valide");
			map.put("loginKO", "*login déjà existant ou non valide");
		}
		if (!ControleSaisie.isNonVide(personne.getAuthentification().getMdp())) {
			ok = false;
			// mv.addObject("mdpKO", "*mot de passe non vide");
			map.put("mdpKO", "*mot de passe non vide");
		}
		if (!ControleSaisie.isNonVide(personne.getAdresse())) {
			ok = false;
			// mv.addObject("adresseKO", "*adresse non vide");
			map.put("adresseKO", "*adresse non vide");
		}
		if (!ControleSaisie.isDateValideWS(personne.getDateDeNaissance())) {
			ok = false;
			// mv.addObject("dateKO", "*age minimum est 16 ans, age maximum 80 ans");
			map.put("dateKO", "*age minimum est 16 ans, age maximum 80 ans");
		}
		if (!ControleSaisie.isNom(personne.getPrenom())) {
			ok = false;
			// mv.addObject("prenomKO", "*uniquement des lettres sans accent");
			map.put("prenomKO", "*uniquement des lettres sans accent");
		}
		if (!ControleSaisie.isNom(personne.getNom())) {
			ok = false;
			// mv.addObject("nomKO", "*uniquement des lettres sans accent");
			map.put("nomKO", "*uniquement des lettres sans accent");
		}

		if (!ok) {
			status = status.BAD_REQUEST;
			// mv.addObject("KO", "erreur données formulaire");
			reponse = map;
		} else {
			status =status.OK;
			reponse = servPers.creationPersonneWS(personne);
			
		}
		return (ResponseEntity<?>) ResponseEntity.status(status).body(reponse);

	}

	@PutMapping(value = "/personne/update")
	// @ResponseStatus(HttpStatus.BAD_REQUEST)
	public Personne updatePersooneWS(@RequestBody Personne personne, HttpStatus status) {

		boolean ok = true;

		if (!ControleSaisie.isMail(personne.getMail())) {
			ok = false;
			// mv.addObject("mailKO", "*mail invalide");
		}
		if (!ControleSaisie.isNumTel(personne.getTel())) {
			ok = false;
			// mv.addObject("telKO", "*10 chiffres sans espace");
		}

		if (!ControleSaisie.isNonVide(personne.getAuthentification().getMdp())) {
			ok = false;
			// mv.addObject("mdpKO", "*mot de passe non vide");
		}
		if (!ControleSaisie.isNonVide(personne.getAdresse())) {
			ok = false;
			// mv.addObject("adresseKO", "*adresse non vide");
		}
		if (!ControleSaisie.isDateValideWS(personne.getDateDeNaissance())) {
			ok = false;
			// mv.addObject("dateKO", "*age minimum est 16 ans, age maximum 80 ans");
		}
		if (!ControleSaisie.isNom(personne.getPrenom())) {
			ok = false;
			// mv.addObject("prenomKO", "*uniquement des lettres sans accent");
		}
		if (!ControleSaisie.isNom(personne.getNom())) {
			ok = false;
			// mv.addObject("nomKO", "*uniquement des lettres sans accent");
		}
		if (servPers.getPersonneByLogin(personne.getAuthentification().getLogin()) == null) {
			ok = false;
			// mv.addObject("nomKO", "*uniquement des lettres sans accent");
		}

		if (!ok) {
			// status=HttpStatus.BAD_REQUEST;
			// mv.addObject("KO", "erreur données formulaire");
		} else {
			personne = servPers.updatePersonneWS(personne);
		}
		return personne;

	}

	@GetMapping(value = "/personne/{login}")
	public Personne getPersonneWS(HttpStatus status, @PathVariable("login") String login) {
		Personne personne = servPers.getPersonneByLogin(login);
		System.out.println(login);
		if (personne != null) {
			return personne;
		} else {
			// status=HttpStatus.BAD_REQUEST;
			return null;
		}

	}
	
	@GetMapping(value = "/personne")
	// @ResponseStatus
	public List<Personne> getListePersonnesWS() {
		return servPers.getAllPersonnes();

	}

	@DeleteMapping(value = "/personne/delete/{login}")
	// @ResponseStatus
	public Personne deletePersonneWS(HttpStatus status, @PathVariable("login") String login) {
		Personne personne = servPers.deletePersonneWS(login);

		if (personne != null) {
			return personne;
		} else {
			// status=HttpStatus.BAD_REQUEST;
			return null;
		}

	}

}
