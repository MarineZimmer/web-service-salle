package fr.afpa.salleafpa.controlleurs;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import fr.afpa.salleafpa.metier.entities.Personne;
import fr.afpa.salleafpa.metier.iservices.IServiceAuthentification;
import fr.afpa.salleafpa.outils.Parametrage;


@Controller
public class AuthentificationController {

	private static final Logger logger = LoggerFactory.getLogger(AuthentificationController.class);

	@Autowired
	private IServiceAuthentification servAuth;

/*	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView authentifGet(ModelAndView mv) {
		mv.setViewName("index");

		return mv;
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ModelAndView authentifPost(ModelAndView mv, @RequestParam(value = "login") String login,
			@RequestParam(value = "mdp") String mdp, HttpSession session) {
		mv.setViewName("index");

		Personne persAuth = null;

		// recuperation de la personne authentifier, null si l'authenfication a échoué
		persAuth = servAuth.authentification(login, mdp);
		if (persAuth != null) { // la pers auth est un admin ou utilisateur
			session.setAttribute("persAuthSalle", persAuth);
			RedirectView redirectView = new RedirectView(Parametrage.URI_VISUALISER_SALLE);
			mv.setView(redirectView);
			
		} else { // authentification incorrecte
			mv.addObject("KO", "Erreur login ou mot de passe incorrects ");
			session.setAttribute("persAuthSalle", null);
			mv.setViewName("index");

		}
		return mv;
	}
	
	@RequestMapping(value = "/deconnexion", method = RequestMethod.GET)
	public ModelAndView deconnexionGet(ModelAndView mv, HttpSession session) {
		session.setAttribute("persAuthSalle", null);
		session.setAttribute("persAuth", null);
		RedirectView redirectView = new RedirectView(Parametrage.URI_AUTHENTIFICATION);
		mv.setView(redirectView);

		return mv;
	}
	
	

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public ModelAndView authentificationAdminGet(ModelAndView mv) {
		
		mv.setViewName("authentificationAdminFinal");
		return mv;
	}
	
	@RequestMapping(value = "/admin", method = RequestMethod.POST)
	public ModelAndView authentificationAdminPost(ModelAndView mv, @RequestParam(value = "loginAdmin") String login,
			@RequestParam(value = "mdpAdmin") String mdp, HttpSession session) {
		mv.setViewName("authentificationAdminFinal");

		Personne persAuth = null;

		// recuperation de la personne authentifier, null si l'authenfication a échoué
		persAuth = servAuth.authentification(login, mdp);
		if (persAuth != null && persAuth.getRole().getId()==Parametrage.ADMIN_ID) { // la pers auth est un admin 
			session.setAttribute("persAuth", persAuth);
			RedirectView redirectView = new RedirectView(Parametrage.URI_ACCUEIL);
			mv.setView(redirectView);
			
		} else { // authentification incorrecte
			mv.addObject("KO", "Erreur login ou mot de passe incorrects, ou utilisateur non autorisé ");
			session.setAttribute("persAuth", null);

		}
		return mv;
	}*/
	
	
}
