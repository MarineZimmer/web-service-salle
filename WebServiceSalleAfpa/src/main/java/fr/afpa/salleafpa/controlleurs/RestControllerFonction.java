package fr.afpa.salleafpa.controlleurs;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.salleafpa.metier.entities.Fonction;
import fr.afpa.salleafpa.metier.iservices.IServiceFonction;

@RestController
//@CrossOrigin(origins = "http://127.0.0.1:5500")
@CrossOrigin(origins = "http://localhost:3000")
public class RestControllerFonction {

	private static final Logger logger = LoggerFactory.getLogger(RestControllerFonction.class);

	@Autowired
	private IServiceFonction servFonction;

	@PostMapping(value = "/fonction/new")
	public Fonction create(@RequestBody Fonction fonction) {
		System.out.println("new fonction");
		//System.out.println(file);
		//System.out.println(fonction);
		return servFonction.create(fonction);
		//return null;

	}

	@PutMapping(value = "/fonction/update")
	public Fonction update(@RequestBody Fonction fonction) {

		return servFonction.update(fonction);

	}

	@GetMapping(value = "/fonction/{id}")
	public Fonction getFonctionWS(@PathVariable("id") Integer id) {
		return servFonction.getFonction(id);
	
	}

	@GetMapping(value = "/fonction")
	public List<Fonction> getListeRolesWS() {
		
		return servFonction.getAllFonction();

	}

	@DeleteMapping(value = "/fonction/delete/{id}")
	public Fonction deleteRoleWS( @PathVariable("id") Integer id) {
		return servFonction.delete(id);


	}

}
