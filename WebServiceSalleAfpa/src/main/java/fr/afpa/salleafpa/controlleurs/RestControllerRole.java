package fr.afpa.salleafpa.controlleurs;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.salleafpa.metier.entities.Role;
import fr.afpa.salleafpa.metier.iservices.IServiceRole;

@RestController

@CrossOrigin(origins = "http://localhost:3000")
public class RestControllerRole {

	private static final Logger logger = LoggerFactory.getLogger(RestControllerRole.class);

	@Autowired
	private IServiceRole servRole;

	@PostMapping(value = "/role/new")
	public Role creationRoleWS(@RequestBody Role role) {
		return servRole.creationRole(role);

	}

	@PutMapping(value = "/role/update")
	
	public Role updateRoleWS(@RequestBody Role role) {

		return servRole.updateRole(role);

	}

	@GetMapping(value = "/role/{id}")
	public Role getRoleWS(@PathVariable("id") Integer id) {
		return servRole.getRole(id);
	
	}

	@GetMapping(value = "/role")
	public List<Role> getListeRolesWS() {
		
		return servRole.getAllRole();

	}

	@DeleteMapping(value = "/role/delete/{id}")
	public Role deleteRoleWS( @PathVariable("id") Integer id) {
		return servRole.deleteRole(id);


	}

}
