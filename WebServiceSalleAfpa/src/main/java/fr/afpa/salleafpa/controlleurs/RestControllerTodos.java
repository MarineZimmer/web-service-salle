package fr.afpa.salleafpa.controlleurs;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.salleafpa.dao.entities.Todo;
import fr.afpa.salleafpa.dao.repositories.TodoRepository;
import fr.afpa.salleafpa.metier.entities.Role;
import fr.afpa.salleafpa.metier.iservices.IServiceRole;

@RestController

@CrossOrigin(origins = "http://localhost:3000")
public class RestControllerTodos {

	private static final Logger logger = LoggerFactory.getLogger(RestControllerTodos.class);

	@Autowired
	private TodoRepository todoRepo;

	@PostMapping(value = "/todos")
	public Todo creationRoleWS(@RequestBody Todo todo) {
		return todoRepo.save(todo);

	}

	@PutMapping(value = "/todos")
	
	public Todo updateRoleWS(@RequestBody Todo todo) {

		return todoRepo.saveAndFlush(todo);

	}

	@GetMapping(value = "/todos/{id}")
	public Todo getRoleWS(@PathVariable("id") Integer id) {
		return todoRepo.findById(id).orElse(null);
	
	}

	@GetMapping(value = "/todos")
	public List<Todo> getListeRolesWS() {
		
		return todoRepo.findAll();

	}

	@DeleteMapping(value = "/todos/{id}")
	public boolean deleteRoleWS( @PathVariable("id") Integer id) {
		try {
			todoRepo.deleteById(id);
			return true;
				}catch (Exception e) {
					return false;
				}
			


	}

}
