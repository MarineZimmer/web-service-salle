package fr.afpa.salleafpa.controlleurs;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import fr.afpa.salleafpa.metier.entities.Authentification;
import fr.afpa.salleafpa.metier.entities.Personne;
import fr.afpa.salleafpa.metier.iservices.IServiceAuthentification;
import fr.afpa.salleafpa.outils.Parametrage;


@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class RestControllerAuthentification {

	private static final Logger logger = LoggerFactory.getLogger(RestControllerAuthentification.class);

	@Autowired
	private IServiceAuthentification servAuth;

	
	@RequestMapping(value = "/auth", method = RequestMethod.POST)
	public Personne authentifPost(@RequestBody Authentification authentification) {
		//mv.setViewName("index");
System.out.println("coucou");
System.out.println(authentification.getLogin());
		Personne persAuth = null;

		// recuperation de la personne authentifier, null si l'authenfication a échoué
		persAuth = servAuth.authentification(authentification.getLogin(),authentification.getMdp());
		if (persAuth != null) { // la pers auth est un admin ou utilisateur
			//session.setAttribute("persAuthSalle", persAuth);
			//RedirectView redirectView = new RedirectView(Parametrage.URI_VISUALISER_SALLE);
			//mv.setView(redirectView);
			
		} else { // authentification incorrecte
			//mv.addObject("KO", "Erreur login ou mot de passe incorrects ");
			//session.setAttribute("persAuthSalle", null);
			//mv.setViewName("index");

		}
		System.out.println(persAuth);
		return persAuth;
	}
	
	@RequestMapping(value = "/deconnexion", method = RequestMethod.GET)
	public ModelAndView deconnexionGet(ModelAndView mv, HttpSession session) {
		session.setAttribute("persAuthSalle", null);
		session.setAttribute("persAuth", null);
		RedirectView redirectView = new RedirectView(Parametrage.URI_AUTHENTIFICATION);
		mv.setView(redirectView);

		return mv;
	}
	
	

	/*@RequestMapping(value = "/admin/auth", method = RequestMethod.GET)
	public ModelAndView authentificationAdminGet(ModelAndView mv) {
		
		mv.setViewName("authentificationAdminFinal");
		return mv;
	}*/
	
	@RequestMapping(value = "/admin/auth", method = RequestMethod.POST)
	public Personne authentificationAdminPost(@RequestBody Authentification authentification) {
		//mv.setViewName("authentificationAdminFinal");

		Personne persAuth = null;

		// recuperation de la personne authentifier, null si l'authenfication a échoué
		persAuth = servAuth.authentification(authentification.getLogin(),authentification.getMdp());
		if (persAuth != null && persAuth.getRole().getId()==Parametrage.ADMIN_ID) { // la pers auth est un admin 
			//session.setAttribute("persAuth", persAuth);
			RedirectView redirectView = new RedirectView(Parametrage.URI_ACCUEIL);
			//mv.setView(redirectView);
			
		} else { // authentification incorrecte
			//mv.addObject("KO", "Erreur login ou mot de passe incorrects, ou utilisateur non autorisé ");
			//session.setAttribute("persAuth", null);

		}
		return persAuth;
	}
	
	
}
