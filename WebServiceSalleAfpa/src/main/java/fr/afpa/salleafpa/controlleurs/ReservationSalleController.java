package fr.afpa.salleafpa.controlleurs;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import fr.afpa.salleafpa.metier.entities.Personne;
import fr.afpa.salleafpa.metier.entities.Reservation;
import fr.afpa.salleafpa.metier.entities.Salle;
import fr.afpa.salleafpa.metier.entities.TypeMateriel;
import fr.afpa.salleafpa.metier.entities.TypeSalle;
import fr.afpa.salleafpa.metier.iservices.IServiceFiltreSalle;
import fr.afpa.salleafpa.metier.iservices.IServiceReservation;
import fr.afpa.salleafpa.metier.iservices.IServiceSalle;
import fr.afpa.salleafpa.metier.iservices.IServiceTypeMateriel;
import fr.afpa.salleafpa.metier.iservices.IServiceTypeSalle;
import fr.afpa.salleafpa.outils.ControleSaisie;
import fr.afpa.salleafpa.outils.Parametrage;


@Controller
public class ReservationSalleController {

	private static final Logger logger = LoggerFactory.getLogger(ReservationSalleController.class);

	@Autowired
	private IServiceSalle servSalle;

	@Autowired
	private IServiceTypeMateriel servTypeMateriel;

	@Autowired
	private IServiceTypeSalle servTypeSalle;

	@Autowired
	private IServiceFiltreSalle servFiltreSalle;

	@Autowired
	private IServiceReservation servReservation;

	/**
	 * controlleur get pour afficher la page de reservation
	 * 
	 * @param mv      : le ModelAndView retourné
	 * @param session : la session
	 * @return : un ModelAndView
	 */
	@RequestMapping(value = "/reserver", method = RequestMethod.GET)
	public ModelAndView reservationSalleGet(ModelAndView mv, HttpSession session) {

		// verifie si une personne est bien authentifié et administrateur
		if (session.getAttribute("persAuthSalle") instanceof Personne
				&& ((Personne) session.getAttribute("persAuthSalle")).getRole().getId() == Parametrage.ADMIN_ID) {
			mv.setViewName("reserverSalle");

			if (!mv.getModel().containsKey("listeSalle")) {
				List<Salle> listeSalle = new ArrayList<Salle>();
				mv.addObject("message", "veuillez remplir le formulaire de réservation");
				mv.addObject("listeSalle", listeSalle);
			} else if (((List<Salle>) mv.getModel().get("listeSalle")).isEmpty()) {
				mv.addObject("message", "aucune salle n'est disponible");
			} else {
				mv.addObject("message", "cliquer sur une salle pour la réserver");
			}

			List<TypeSalle> listeTypeSalle = servTypeSalle.getAll();
			mv.addObject("listeTypeSalle", listeTypeSalle);

			List<TypeMateriel> listeTypeMateriel = servTypeMateriel.getAllTypeMateriel();
			mv.addObject("listeTypeMateriel", listeTypeMateriel);

		} else { // non authentifier retour à la page authentification
			RedirectView redirectView = new RedirectView(Parametrage.URI_AUTHENTIFICATION);
			mv.setView(redirectView);
		}
		return mv;

	}

	/**
	 * controlleur post pour filtrer les salles disponibles selon les critères de
	 * reservation
	 * 
	 * @param mv           : ModelAndView
	 * @param session      : la session
	 * @param dateDebut    : la date de début de la réservation
	 * @param dateFin      : la date de fin de la réservation
	 * @param nom          : le nom de la réservation
	 * @param capacite     : la capacité minimum de la salle à réserver
	 * @param tabTypeSalle : les types de salle souhaités
	 * @param params       : la liste des parametres (pour récupérer la liste de
	 *                     materiel et la quantité)
	 * @return
	 */
	@RequestMapping(value = "/reserver", method = RequestMethod.POST)
	public ModelAndView reservationSallePost(ModelAndView mv, HttpSession session,
			@RequestParam(value = "dateDebut") String dateDebut, @RequestParam(value = "dateFin") String dateFin,
			@RequestParam(value = "nom") String nom, @RequestParam(value = "capacite") String capacite,
			@RequestParam(value = "typeSalle") String[] tabTypeSalle, @RequestParam Map<String, String> params) {


		mv.addAllObjects(params);
		boolean ok = true;

		if (!ControleSaisie.isDateReservation(dateDebut, dateFin)) {
			ok = false;
			mv.addObject("dateKO", "* Dates invalides");
		}

		if (!ControleSaisie.isNonVide(nom)) {
			ok = false;
			mv.addObject("nomKO", "*veuillez entrer un nom de réservation");
		}

		if (ok) {
			List<Salle> listeSalle = servSalle.getAllSalles();

			// filtre salle actif
			listeSalle = servFiltreSalle.filtreActif(listeSalle, true);

			// filtre date salle dispo
			listeSalle = servFiltreSalle.filtreDateSalle(listeSalle, LocalDate.parse(dateDebut),
					LocalDate.parse(dateFin), true);

			// filtre capacite
			if (!"".equals(capacite)) {
				listeSalle = servFiltreSalle.filtreCapacite(listeSalle, Integer.parseInt(capacite));
			}

			// filtre type salle
			if (tabTypeSalle != null && tabTypeSalle.length != 0) {
				List<Integer> listeTypeSalle = Arrays.asList(tabTypeSalle).stream().map(Integer::parseInt)
						.collect(Collectors.toList());
				listeSalle = servFiltreSalle.filtreTypeSalle(listeSalle, listeTypeSalle);
			}

			// filtre materiel
			for (Entry<String, String> param : params.entrySet()) {
				if ("tm".equals(param.getKey().substring(0, 2)) && !"".equals(param.getValue())) {
					listeSalle = servFiltreSalle.filtreMateriel(listeSalle,
							Integer.parseInt(param.getKey().substring(2)), Integer.parseInt(param.getValue()));
				}
			}

			mv.addObject("listeSalle", listeSalle);
		}
		return reservationSalleGet(mv, session);
	}

	/**
	 * controlleur d'enregistrement d'une réservation
	 * 
	 * @param mv        le ModelAndView
	 * @param session   : la session
	 * @param id        : l'id de la salle à réserver
	 * @param dateDebut : la date de début
	 * @param dateFin   : la date fin
	 * @param nom       : le nom de la réservation
	 * @return : le ModelAndView correspondant au récapitulatif de la réservation
	 */
	@RequestMapping(value = "/reserver/salle/{id}/{dateDebut}/{dateFin}/{nom}", method = RequestMethod.GET)
	public ModelAndView creationReservationGet(ModelAndView mv, HttpSession session, @PathVariable("id") String id,
			@PathVariable("dateDebut") String dateDebut, @PathVariable("dateFin") String dateFin,
			@PathVariable("nom") String nom) {

		// verifie si une personne est bien authentifié
		if (session.getAttribute("persAuthSalle") instanceof Personne
				&& ((Personne) session.getAttribute("persAuthSalle")).getRole().getId() == Parametrage.ADMIN_ID) {

			boolean ok = true;
			Salle salle = null;

			if (!ControleSaisie.isDateReservation(dateDebut, dateFin)) {
				ok = false;
				mv.addObject("dateKO", "* Dates invalides");
			}

			if (ControleSaisie.isIntegerPositif(id)) {
				salle = servSalle.getSalle(Integer.parseInt(id));
			}
			if (salle == null) {
				ok = false;
			} else {
				if (servFiltreSalle.etatSalle(salle, LocalDate.parse(dateDebut), LocalDate.parse(dateFin), false)) {
					ok = false;
					mv.addObject("occupeKO", "* salle occupée durant cette période");
				}
				if (!salle.isActif()) {
					ok = false;
					mv.addObject("actifKO", "* salle inactif");
				}
			}

			if (!ControleSaisie.isNonVide(nom)) {
				ok = false;
				mv.addObject("nomKO", "* veuillez entrer un nom de réservation");
			}
			if (ok) {
				Reservation reservation = new Reservation(0, LocalDate.parse(dateDebut), LocalDate.parse(dateFin), nom);
				reservation.setSalle(salle);
				reservation = servReservation.createReservation(reservation);
				if (reservation != null) {
					RedirectView redirectView = new RedirectView(
							Parametrage.URI_VISUALISER_RESERVATION + "/" + reservation.getIdReservation());
					mv.setView(redirectView);
				}

			} else {
				return reservationSalleGet(mv, session);
			}

		} else { // non authentifier retour à la page authentification
			RedirectView redirectView = new RedirectView(Parametrage.URI_AUTHENTIFICATION);
			mv.setView(redirectView);
		}
		return mv;

	}

	/**
	 * Controlleur pour afficher le recapitulatif de la reservation
	 * 
	 * @param mv      : ModelAndView
	 * @param session : la session
	 * @param id      : id de la reservation
	 * @return : ModelAndView correspondant au récapitulatif de la réservation
	 */
	@RequestMapping(value = "/visualiser/reservation/{id}", method = RequestMethod.GET)
	public ModelAndView visualiserReservationGet(ModelAndView mv, HttpSession session, @PathVariable("id") String id) {

		// verifie si une personne est bien authentifié pour accéder au menu
		if (session.getAttribute("persAuthSalle") instanceof Personne
				&& ((Personne) session.getAttribute("persAuthSalle")).getRole().getId() == Parametrage.ADMIN_ID) {

			mv.setViewName("visualisationReservation");
			if (ControleSaisie.isIntegerPositif(id)) {
				Reservation reservation = servReservation.getById(Integer.parseInt(id));
				if (reservation != null) {
					mv.addObject("reservation", reservation);
				}

			}

		} else { // non authentifier retour à la page authentification
			RedirectView redirectView = new RedirectView(Parametrage.URI_AUTHENTIFICATION);
			mv.setView(redirectView);
		}
		return mv;

	}

	/**
	 * Controlleur post modification reservation
	 * 
	 * @param mv        : le modelAndView
	 * @param session   : la session
	 * @param dateDebut : la date de début de la réservation
	 * @param dateFin:  la date de fin de la réservation
	 * @param nom       : le nom de la réservation
	 * @param id        : l'id de la réservation
	 * @return le ModelAndView de la visualisation de la réservation
	 */
	@RequestMapping(value = "/visualiser/reservation/modification", method = RequestMethod.POST)
	public ModelAndView modificationReservationPost(ModelAndView mv, HttpSession session,
			@RequestParam(value = "dateDebut") String dateDebut, @RequestParam(value = "dateFin") String dateFin,
			@RequestParam(value = "nom") String nom, @RequestParam(value = "id") String id) {

		// verifie si une personne est bien authentifié pour accéder au menu
		if (session.getAttribute("persAuthSalle") instanceof Personne
				&& ((Personne) session.getAttribute("persAuthSalle")).getRole().getId() == Parametrage.ADMIN_ID) {
			mv.setViewName("visualisationReservation");

			boolean ok = true;
			Reservation reservation = null;

			if (!ControleSaisie.isNonVide(nom)) {
				ok = false;
				mv.addObject("nomKO", "* veuillez entrer un nom de réservation");
			}

			if (ControleSaisie.isIntegerPositif(id)) {
				reservation = servReservation.getById(Integer.parseInt(id));
			}

			if (reservation != null) {
				if (!ControleSaisie.isDateReservationModification(dateDebut, dateFin, reservation.getDateDebut())) {
					ok = false;
					mv.addObject("dateKO", "* Dates invalides");
				}

				reservation.getSalle().getListeReservation().remove(reservation);
				if (servFiltreSalle.etatSalle(reservation.getSalle(), LocalDate.parse(dateDebut),
						LocalDate.parse(dateFin), false)) {
					ok = false;
					mv.addObject("reservationKO", "La salle est déjà occupée durant cette période");
				}

			
			if ( ok) {
				reservation.setDateDebut(LocalDate.parse(dateDebut));
				reservation.setDateFin(LocalDate.parse(dateFin));
				reservation.setNomReservation(nom);
				reservation.getSalle().getListeReservation().add(reservation);
				reservation = servReservation.update(reservation);
				mv.addObject("modifKO", "la reservation a été modifié");

			} else {
				mv.addObject("modifKO", "Désolé la reservation n'a pas été modifié");
			}
			
			return visualiserReservationGet(mv, session, "" + reservation.getIdReservation());
			
			}else {
				RedirectView redirectView = new RedirectView(Parametrage.URI_AUTHENTIFICATION);
				mv.setView(redirectView);
			}
		} else { // non authentifier retour à la page authentification
			RedirectView redirectView = new RedirectView(Parametrage.URI_AUTHENTIFICATION);
			mv.setView(redirectView);
		}
		return mv;
	}

	/**
	 * controlleur get pour afficher la page de reservation
	 * 
	 * @param mv      : le ModelAndView retourné
	 * @param session : la session
	 * @return : un ModelAndView
	 */
	@RequestMapping(value = "/listeReservation/{page}", method = RequestMethod.GET)
	public ModelAndView listeReservationGet(ModelAndView mv, HttpSession session, @PathVariable("page") String page) {

		// verifie si une personne est bien authentifié et administrateur
		if (session.getAttribute("persAuthSalle") instanceof Personne
				&& ((Personne) session.getAttribute("persAuthSalle")).getRole().getId() == Parametrage.ADMIN_ID) {
			mv.setViewName("listeReservation");

			int nbReservPage = 1;
			if (ControleSaisie.isIntegerPositif(page)) {
				nbReservPage = Integer.parseInt(page) - 1;
			}
			if (nbReservPage < 0) {
				nbReservPage = 0;
			}
			int nbPage = servReservation.nbPageListeReservation(Parametrage.NB_RESERV_PAGE);
			System.out.println(nbPage);

			List<Reservation> listeReservation = servReservation.getAll(nbReservPage, Parametrage.NB_RESERV_PAGE);
			mv.addObject("listeReservation", listeReservation);

			mv.addObject("nbPage", nbPage);

		} else { // non authentifier retour à la page authentification
			RedirectView redirectView = new RedirectView(Parametrage.URI_AUTHENTIFICATION);
			mv.setView(redirectView);
		}
		return mv;

	}

	/**
	 * Controlleur pour supprimer une reservation
	 * 
	 * @param mv      : ModelAndView
	 * @param session : la session
	 * @param id      : id de la reservation
	 * @return : ModelAndView correspondant au récapitulatif de la réservation
	 */
	@RequestMapping(value = "/deletereservation/{id}", method = RequestMethod.GET)
	public ModelAndView deleteReservationGet(ModelAndView mv, HttpSession session, @PathVariable("id") String id) {

		// verifie si une personne est bien authentifié pour accéder au menu
		if (session.getAttribute("persAuthSalle") instanceof Personne
				&& ((Personne) session.getAttribute("persAuthSalle")).getRole().getId() == Parametrage.ADMIN_ID) {

			mv.setViewName("visualisationReservation");
			if (ControleSaisie.isIntegerPositif(id)) {
				Reservation reservation = servReservation.getById(Integer.parseInt(id));
				if (reservation == null) {
					mv.addObject("message", "la réservation n'existe pas ");
				} else if (reservation.getDateDebut().isAfter(LocalDate.now())) {
					servReservation.deleteById(Integer.parseInt(id));
					mv.addObject("message", "la reservation a été supprimé");
				} else {
					mv.addObject("message", "une reservation en cours ne peux pas être supprimé");
				}
			}
			mv.setViewName("messageSuppressionReservation");

		} else { // non authentifier retour à la page authentification
			RedirectView redirectView = new RedirectView(Parametrage.URI_AUTHENTIFICATION);
			mv.setView(redirectView);
		}
		return mv;

	}

}
