package fr.afpa.salleafpa.controlleurs;

import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import fr.afpa.salleafpa.metier.entities.Personne;
import fr.afpa.salleafpa.metier.iservices.IServicePersonne;
import fr.afpa.salleafpa.outils.ControleSaisie;
import fr.afpa.salleafpa.outils.Parametrage;

@Controller
public class AdminController {

	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);

	@Autowired
	private IServicePersonne servPers;

	/**
	 * Controller get pour l'affichage de l'acceuil
	 * 
	 * @param mv      : le ModelAndView
	 * @param session : la session
	 * @return un ModelAndView
	 */
	@RequestMapping(value = "/admin/accueil", method = RequestMethod.GET)
	public ModelAndView accueilGet(ModelAndView mv, HttpSession session) {

		// verifie si une personne est bien authentifié pour accéder au menu
		if (session.getAttribute("persAuth") instanceof Personne) {
			mv.setViewName("pageAccueil");

		} else { // non authentifier retour à la page authentification
			RedirectView redirectView = new RedirectView(Parametrage.URI_AUTHENTIFICATION);
			mv.setView(redirectView);
		}
		return mv;
	}

	/**
	 * Controller get pour la creation d'une personne
	 * 
	 * @param mv      : le ModelAndView
	 * @param session : la session
	 * @return un ModelAndView
	 */
	@RequestMapping(value = "/admin/accueil/creation", method = RequestMethod.GET)
	public ModelAndView creationUserGet(ModelAndView mv, HttpSession session) {

		// verifie si une personne est bien authentifié pour accéder au menu
		if (session.getAttribute("persAuth") instanceof Personne) {
			mv.setViewName("pageCreationUser");

		} else { // non authentifier retour à la page authentification
			RedirectView redirectView = new RedirectView(Parametrage.URI_AUTHENTIFICATION);
			mv.setView(redirectView);
		}

		return mv;
	}

	/**
	 * Controller post pour la creation d'une personne
	 * 
	 * @param mv      : le ModelAndView
	 * @param session : la session
	 * @param params  : la Map des paramètres réçu de la jsp
	 * @return un ModelAndView
	 */
	@RequestMapping(value = "/admin/accueil/creation", method = RequestMethod.POST)
	public ModelAndView creationUserPost(ModelAndView mv, HttpSession session,
			@RequestParam Map<String, String> params) {

		RequestDispatcher requestDispatcher;
		String uri = null;

		boolean ok = true;
		mv.addObject("mail", params.get("mail"));
		mv.addObject("telephone", params.get("telephone"));
		mv.addObject("login", params.get("login"));
		mv.addObject("nom", params.get("nom"));
		mv.addObject("prenom", params.get("prenom"));
		mv.addObject("mdp", params.get("mdp"));
		mv.addObject("role", params.get("role"));
		mv.addObject("fonction", params.get("fonction"));
		mv.addObject("adresse", params.get("adresse"));
		mv.addObject("date", params.get("date"));
		mv.addObject("role", Integer.parseInt(params.get("role")));

		if (!ControleSaisie.isMail(params.get("mail"))) {
			ok = false;
			mv.addObject("mailKO", "*mail invalide");
		}
		if (!ControleSaisie.isNumTel(params.get("telephone"))) {
			ok = false;
			mv.addObject("telKO", "*10 chiffres sans espace");
		}
		if (!ControleSaisie.isNonVide(params.get("login"))
				|| servPers.getPersonneByLogin(params.get("login")) != null) {
			ok = false;
			mv.addObject("loginKO", "*login déjà existant ou non valide");
		}
		if (!ControleSaisie.isNonVide(params.get("mdp"))) {
			ok = false;
			mv.addObject("mdpKO", "*mot de passe non vide");
		}
		if (!ControleSaisie.isNonVide(params.get("adresse"))) {
			ok = false;
			mv.addObject("adresseKO", "*adresse non vide");
		}
		if (!ControleSaisie.isDateValide(params.get("date"))) {
			ok = false;
			mv.addObject("dateKO", "*age minimum est 16 ans, age maximum 80 ans");
		}
		if (!ControleSaisie.isNom(params.get("prenom"))) {
			ok = false;
			mv.addObject("prenomKO", "*uniquement des lettres sans accent");
		}
		if (!ControleSaisie.isNom(params.get("nom"))) {
			ok = false;
			mv.addObject("nomKO", "*uniquement des lettres sans accent");
		}

		if (!ok) {
			mv.setViewName("pageCreationUser");
			mv.addObject("KO", "erreur données formulaire");
		} else {
			servPers.creationPersonne(params.get("nom"), params.get("prenom"), params.get("mail"),
					params.get("telephone"), params.get("adresse"), params.get("date"), params.get("role"),
					params.get("fonction"), params.get("login"), params.get("mdp"), true);
			RedirectView redirectView = new RedirectView(Parametrage.URI_ACCUEIL);
			mv.getModelMap().clear();
			mv.setView(redirectView);
		}

		return mv;
	}

	/**
	 * Controller get pour l'affichage de tous les utilisateurs existants
	 * 
	 * @param mv      : le ModelAndView
	 * @param session : la session
	 * @return un ModelAndView
	 */
	@RequestMapping(value = "/admin/accueil/recherche", method = RequestMethod.GET)
	public ModelAndView rechercheUserGet(ModelAndView mv, HttpSession session) {
		RequestDispatcher requestDispatcher;
		String uri = "admin";

		// verifie si une personne est bien authentifié pour accéder au menu
		if (session.getAttribute("persAuth") instanceof Personne) {
			mv.setViewName("rechercheFinal");
			List<Personne> listePersRech = servPers.getAllPersonnes();
			mv.addObject("listePers", listePersRech);

		} else { // non authentifier retour à la page authentification
			RedirectView redirectView = new RedirectView(Parametrage.URI_AUTHENTIFICATION);
			mv.setView(redirectView);
		}
		return mv;
	}

	/**
	 * Controller post pour l'affichage de des informations d'une personne
	 * 
	 * @param mv      : le ModelAndView
	 * @param session : la session
	 * @param login   : le login de la personne à rechercher
	 * @return un ModelAndView
	 */
	@RequestMapping(value = "/admin/accueil/recherche", method = RequestMethod.POST)
	public ModelAndView rechercheUserPost(ModelAndView mv, HttpSession session,
			@RequestParam(value = "login") String login) {

		if (login != null) {
			Personne personne = servPers.getPersonneByLogin(login);
			if (personne != null) {
				mv.setViewName("pageInfoUser");
				mv.addObject("pers", personne);
			} else {
				RedirectView redirectView = new RedirectView(Parametrage.URI_RECHERCHE);
				mv.setView(redirectView);
			}
		} else {
			RedirectView redirectView = new RedirectView(Parametrage.URI_RECHERCHE);
			mv.setView(redirectView);
		}
		return mv;
	}

	/**
	 * Controller post pour la suppression personne
	 * 
	 * @param mv      : le ModelAndView
	 * @param session : la session
	 * @param login   : le login de la personne à supprimer
	 * @return un ModelAndView
	 */
	@RequestMapping(value = "/admin/accueil/supprimer", method = RequestMethod.POST)
	public ModelAndView suppressionPersonnePost(ModelAndView mv, HttpSession session,
			@RequestParam(value = "login") String login) {
		boolean result = servPers.deletePersonne(login);
		if (result) {
			mv.addObject("message", "La Personne a été supprimée");
		} else {
			mv.addObject("message",
					"Erreur  la Personne n'a pas été supprimée, on ne peut pas supprimer un administrateur");
		}
		mv.setViewName("suppression");
		return mv;
	}

	/**
	 * Controller post pour la gestion d'une personne existante
	 * 
	 * @param mv      : le ModelAndView
	 * @param session : la session
	 * @param params  : la Map des paramètres réçu de la jsp
	 * @return un ModelAndView
	 */
	@RequestMapping(value = "/admin/accueil/gestion", method = RequestMethod.POST)
	public ModelAndView gestionModifUserPost(ModelAndView mv, HttpSession session,
			@RequestParam Map<String, String> params) {
		if (params.containsKey("modifier") || params.containsKey("supprimer")) {
			if (params.containsKey("modifier")) {
				return modifierPersonneGet(mv, session, params.get("modifier"));

			} else {
				return suppressionPersonnePost(mv, session, params.get("supprimer"));
			}

		} else {
			RedirectView redirectView = new RedirectView(Parametrage.URI_ACCUEIL);
			mv.setView(redirectView);

		}
		return mv;
	}

	/**
	 * Controller post affichage modification personne
	 * 
	 * @param mv      : le ModelAndView
	 * @param session : la session
	 * @param login   : le login de la personne à afficher
	 * @return un ModelAndView : le ModelAndView
	 */
	@RequestMapping(value = "/admin/accueil/modifier", method = RequestMethod.POST)
	public ModelAndView modifierPersonneGet(ModelAndView mv, HttpSession session,
			@RequestParam(value = "login") String login) {

		Personne personne = null;

		if (session.getAttribute("persAuth") instanceof Personne) {

			if (login != null) {
				personne = servPers.getPersonneByLogin(login);
			}

			if (personne != null) {
				mv.addObject("pers", personne);
				mv.setViewName("modificationUser");

			}
		} else { // retour à la page authentification
			RedirectView redirectView = new RedirectView(Parametrage.URI_RECHERCHE);
			mv.setView(redirectView);
		}

		return mv;
	}

	/**
	 * Controller post modification personne
	 * 
	 * @param mv      : le ModelAndView
	 * @param session : la session
	 * @param params  : la Map des paramètres réçu de la jsp
	 * @return un ModelAndView
	 */
	@RequestMapping(value = "/admin/accueil/recherche/modifier", method = RequestMethod.POST)
	public ModelAndView modifierPersonnePost(ModelAndView mv, HttpSession session,
			@RequestParam Map<String, String> params) {

		RequestDispatcher requestDispatcher;
		String uri = null;

		if (params.containsKey("log")) {

			boolean ok = true;
			if (!ControleSaisie.isMail(params.get("mail"))) {
				ok = false;
				mv.addObject("mailKO", "*mail invalide");
			}
			if (!ControleSaisie.isNumTel(params.get("telephone"))) {
				ok = false;
				mv.addObject("telKO", "*10 chiffres sans espace");
			}

			if (!ControleSaisie.isNonVide(params.get("mdp"))) {
				ok = false;
				mv.addObject("mdpKO", "*mot de passe non vide");
			}
			if (!ControleSaisie.isNonVide(params.get("adresse"))) {
				ok = false;
				mv.addObject("adresseKO", "*adresse non vide");
			}
			if (!ControleSaisie.isDateValide(params.get("date"))) {
				ok = false;
				mv.addObject("dateKO", "*age minimum est 16 ans, age maximum 80 ans");
			}
			if (!ControleSaisie.isNom(params.get("prenom"))) {
				ok = false;
				mv.addObject("prenomKO", "*uniquement des lettres sans accent");
			}
			if (!ControleSaisie.isNom(params.get("nom"))) {
				ok = false;
				mv.addObject("nomKO", "*uniquement des lettres sans accent");
			}

			if (!ok) {
				mv.addObject("KO", "Erreur saisie");
				return modifierPersonneGet(mv, session, params.get("log"));

			} else {
				uri = "admin/accueil";
				servPers.updatePers(params.get("nom"), params.get("prenom"), params.get("mail"),
						params.get("telephone"), params.get("adresse"), params.get("date"), params.get("actif"),
						params.get("log"), params.get("role"), params.get("fonction"));

				RedirectView redirectView = new RedirectView(Parametrage.URI_ACCUEIL);
				mv.setView(redirectView);
			}
		} else {
			RedirectView redirectView = new RedirectView(Parametrage.URI_RECHERCHE);
			mv.setView(redirectView);
		}

		return mv;
	}

}
