package fr.afpa.salleafpa.controlleurs;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.salleafpa.metier.entities.Role;
import fr.afpa.salleafpa.metier.entities.TypeMateriel;
import fr.afpa.salleafpa.metier.iservices.IServiceTypeMateriel;

@RestController

@CrossOrigin(origins = "http://localhost:3000")
public class RestControllerTypeMateriel {

	private static final Logger logger = LoggerFactory.getLogger(RestControllerTypeMateriel.class);

	@Autowired
	private IServiceTypeMateriel servtypeMateriel;

	@PostMapping(value = "/typemateriel")
	public TypeMateriel creationTypeMaterielWS(@RequestBody TypeMateriel typeMateriel) {
		return servtypeMateriel.creationTypeMateriel(typeMateriel);

	}

	@PutMapping(value = "/typemateriel")
	public TypeMateriel updateTypeMaterielWS(@RequestBody TypeMateriel typeMateriel) {
		System.out.println(typeMateriel.getLibelle());
		System.out.println(servtypeMateriel.updateTypeMateriel(typeMateriel).getLibelle());
		return servtypeMateriel.updateTypeMateriel(typeMateriel);

	}

	@GetMapping(value = "/typemateriel/{id}")
	public TypeMateriel getTypeMaterielWS(@PathVariable("id") Integer id) {
		return servtypeMateriel.getTypeMateriel(id);
	
	}

	@GetMapping(value = "/typemateriel")
	public List<TypeMateriel> getListeTypeMaterielsWS() {
		return servtypeMateriel.getAllTypeMateriel();

	}

	@DeleteMapping(value = "/typemateriel/{id}")
	public TypeMateriel deleteTypeMaterielWS( @PathVariable("id") Integer id) {
		return servtypeMateriel.deleteTypeMateriel(id);


	}

}
