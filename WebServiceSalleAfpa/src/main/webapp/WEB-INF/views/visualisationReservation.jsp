<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="fr.afpa.salleafpa.outils.Parametrage"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link href="${pageContext.request.contextPath}/resources/css/menu.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/cssVisualisationSalle.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Visualisation</title>
</head>
<body>
<body>
	<c:choose>
		<c:when test="${not empty sessionScope.persAuthSalle  }">
			<jsp:include page="menu.jsp" />
			<div class="container">

				<h2>R�capitulatif r�servation :</h2>

				<c:if test="${not empty reservation}">
				
						
					
					<div class="colonneGauche "></div>
					<div class="colonneDroite">
						<em class="erreur">${modifKO }</em> <br> <em class="erreur">${reservationKO }</em>

					</div>

					<form action="modification" method="post">
						<input type="hidden" name="id"
							value="${reservation.idReservation }">


						<div class="colonneGauche ">
							<label>Reservation:</label>
						</div>
						<div class="colonneDroite">
							<input type="text" name="nom"
								value="${reservation.nomReservation }"> <br> <em
								class="erreur">${nomKO}</em>

						</div>

						<div class="colonneGauche">
							<label>Date de d�but :</label>
						</div>
						<div class="colonneDroite">
							<input type="date" name="dateDebut"
								value="${reservation.dateDebut }">
						</div>
						<!--partie mail-->
						<div class="colonneGauche">
							<label>Date de fin :</label>
						</div>
						<div class="colonneDroite">
							<input type="date" name="dateFin" value="${reservation.dateFin }">
							<br> <em class="erreur">${dateKO}</em>
						</div>


						<div class="colonneGauche">
							<label>Salle reserv�e :</label>
						</div>
						<div class="colonneDroite">
							<label>${reservation.salle.nom }</label>
						</div>

						<div class="colonneGauche">
							<label>Batiment :</label>
						</div>
						<div class="colonneDroite">
							<label>${reservation.salle.batiment.nom }</label>
						</div>

						<div class="colonneGauche">
							<label>Numero :</label>
						</div>
						<div class="colonneDroite">
							<label>${reservation.salle.numero }</label>
						</div>

						<div class="colonneGauche"></div>
						<div class="colonneDroite">
							<input type="submit" value="Modifier">
							<br>
							<br>
							<a
							href="${ Parametrage.URI_SUPPRIMER_RESERVATION}/${reservation.idReservation }">supprimer la reservation(si elle n'est pas en cours)</a>
						</div>



					</form>
					
				</c:if>
			</div>

		</c:when>
		<c:otherwise>
			<c:redirect url=""></c:redirect>
		</c:otherwise>
	</c:choose>
	<script
		src="${pageContext.request.contextPath}/resources/js/modifReservation.js"></script>
</body>
</html>