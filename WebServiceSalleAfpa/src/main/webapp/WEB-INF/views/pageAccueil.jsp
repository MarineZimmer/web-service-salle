<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="org.omg.CORBA.portable.ApplicationException"%>
<%@page import="fr.afpa.salleafpa.outils.Parametrage"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>accueil</title>
<link
	href="${pageContext.request.contextPath}/resources/css/cssPageAccueilChoix.css"
	rel="stylesheet">
</head>
<body>
	<c:choose>
		<c:when
			test="${sessionScope.persAuth.role.id eq Parametrage.ADMIN_ID }">
			<flex-container>
			<form method="post">
				<label id="labelInfo">Bonjour ${sessionScope.persAuth.prenom } ${sessionScope.persAuth.nom}</label> <br> <a
					href="${ Parametrage.URI_NEW_USER }"><button type="button"
						value="NewUser">Cr�er un Nouvel Utilisateur</button></a> <br> <br>
				<br> <a href="${ Parametrage.URI_RECHERCHE }"><button
						type="button" value="GestionUserexistant">Gestion d'un
						Utilisateur Existant</button></a> <br> <br> <br> <br> <br>
				<a href="${ Parametrage.URI_DECONNEXION }"><button type="button"
						value="Deconnexion">D�connexion</button></a>
			</form>
			</flex-container>
		</c:when>
		<c:otherwise>
			<c:redirect url=""></c:redirect>
		</c:otherwise>
	</c:choose>
</body>
</html>