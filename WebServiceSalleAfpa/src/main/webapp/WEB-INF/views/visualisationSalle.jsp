<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="fr.afpa.salleafpa.outils.Parametrage"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link href="${pageContext.request.contextPath}/resources/css/menu.css" rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/cssVisualisation.css"
	rel="stylesheet">
	
<meta charset="ISO-8859-1">
<title>Visualisation</title>
</head>
<body>
	<c:choose>
		<c:when test="${not empty sessionScope.persAuthSalle  }">
			<jsp:include page="menu.jsp" />
			<div class="container">
				<div class="colonne">
				
						<table class="colonne">

							<c:forEach var="salle" items="${listeSalle }" varStatus="status">
								<c:if test="${ status.index % 4 == 0}">
									<tr>
								</c:if>
								<td><a href="${Parametrage.URI_VISUALISER_UNE_SALLE }?id=${salle.idSalle}">${ salle.nom } <br> Batiment : ${ salle.batiment.nom },
										n� ${ salle.numero }<br> type : ${ salle.typeSalle.libelle }<br>
										etage ${ salle.etage }<br> capacit� : ${ salle.capacite }
								</a></td>
								<c:if test="${status.index+1 % 4 == 0 || status.last}">
									</tr>
								</c:if>
							</c:forEach>

						</table>
				</div>
				
				<div class="colonne2">
				<h2>Recherche</h2>
				<form method="post"  action="${ Parametrage.URI_VISUALISER_SALLE}">
				
				<!-- filtre actif -->
				<div>
				<input  name="actif" value= "true" type="radio">
				<label >active     </label>
				<input  name="actif" value= "false" type="radio">
				<label>non active      </label>
				<input  name="actif" value= "nonFiltre" type="radio">
				<label>active/non active</label>
				
				<!-- filtre date -->
				<div>
				<input id="cbReserve" name="date" value= "reserve" type="radio">
				<label >reserv�     </label>
				<input id="cblibre" name="date" value= "libre" type="radio">
				<label>libre      </label>
				<input id="cblibre" name="date" value= "nonFiltre" type="radio">
				<label>libre/reserv�</label>
				
				
				<br>
				</div>
				<div>
				<label >Date debut : </label>
				<input id="dateDebut" name="dateDebut" type="date">
				<br>
				<label >Date fin : </label>
				<input id="dateFin" name="dateFin" type="date">
				<br>
					<!-- Capacite min -->
				<label >Capacit� min : </label>
				<input id="capacite" name="capacite" type="number">
				<br>
				
				<!-- filtre type salle -->
				<input type="text" value="0" name="typeSalle" hidden="">
				<c:forEach var="typeSalle" items="${listeTypeSalle }">
				<input type="checkbox" value="${typeSalle.id }" name="typeSalle">
                              <label>${typeSalle.libelle }</label>
                              </c:forEach>
                        <br>
                         <!--filtre type materiel-->
                        <c:forEach var= "typeMateriel" items="${listeTypeMateriel }">
                       
                            <label>${typeMateriel.libelle } :</label>
                             <input id ="${typeMateriel.idTypeMateriel}" name="tm${typeMateriel.idTypeMateriel}" value="" type="number" size="3">
                          <br>
                           </c:forEach>
                           
				<br>
				
				<input type="submit" value="Recherche">
				</div>
				</form>
				</div>
					
		</c:when>
		<c:otherwise>
			<c:redirect url=""></c:redirect>
		</c:otherwise>
	</c:choose>

</body>
</html>