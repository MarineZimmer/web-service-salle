<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="fr.afpa.salleafpa.outils.Parametrage"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/menu.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/cssListeReservation.css"
	rel="stylesheet">
<title>liste reservation</title>
</head>
<body>
	<jsp:include page="menu.jsp" />
	<div class="container">
		<div>
			<h1>liste reservation</h1>
		</div>

		<div class="container-fluid col-sm-12">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Nom</th>
						<th>Date de d�but</th>
						<th>Date de fin</th>
						<th>Nom salle</th>
						<th>Batiment</th>
						<th>Numero</th>
						<th></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${listeReservation}" var="reservation">
						<tr>
							<td>${reservation.nomReservation}</td>
							<td><fmt:parseDate value="${reservation.dateDebut}"
									type="date" pattern="yyyy-MM-dd" var="parsedDate" /> <fmt:formatDate
									value="${parsedDate}" type="date" pattern="dd/MM/yyyy"
									var="dateDeb" /> ${ dateDeb}</td>
							<td><fmt:parseDate value="${reservation.dateFin}"
									type="date" pattern="yyyy-MM-dd" var="parsedDate" /> <fmt:formatDate
									value="${parsedDate}" type="date" pattern="dd/MM/yyyy"
									var="dateFin" /> ${ dateFin}</td>

							<td>${reservation.salle.nom}</td>
							<td>${reservation.salle.batiment.nom}</td>
							<td>${reservation.salle.numero}</td>
							<td><a
								href="${ Parametrage.URI_VISUALISER_RESERVATION}/${reservation.idReservation }">modifier</a></td>
							<td><a
								href="${ Parametrage.URI_SUPPRIMER_RESERVATION}/${reservation.idReservation }"">supprimer</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<nav>
				<ul class="pagination">
					<c:forEach var="i" begin="1" end="${ nbPage }" step="1">

						<li class="page-item"><a class="page-link"
							href="${ Parametrage.URI_LISTE_RESERVATION}/${ i}">${ i }</a></li>

					</c:forEach>
				</ul>
			</nav>
		</div>



	</div>

	<script
		src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.slim.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
</body>
</html>